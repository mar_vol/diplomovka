uniform float TessLevel;

int OsdPrimitiveIdBase()
{
    return 0;
}

int OsdGregoryQuadOffsetBase()
{
    return 0;
}

int OsdBaseVertex()
{
    return 0;
}

#ifdef VERTEX_SHADER

in vec3 position;
in vec2 uv;
out vec3 pos;


out UV_DATA
{
    vec2 uv;
}uv_out;

void main()
{
	gl_Position = vec4(position, 1.0);
    pos = position;
    uv_out.uv = uv;

}

#endif
//////////////////////////////////////////////
#ifdef TESS_CONTROL_SHADER

layout (vertices = 16) out;
in vec3 pos[];

in UV_DATA
{
    vec2 uv;
}uv_in[];

out UV_DATA_TES
{
    vec2 uv;
}uv_out[];

patch out vec4 tessOuterLo[1], tessOuterHi[1];
out OsdPerPatchVertexBezier v[];
out vec4 l[];

uniform float tessFactor;

float OsdTessLevel()
{
    return tessFactor;
}

void main()
{
	// Get a patch param from texture buffer.
	ivec3 patchParam = OsdGetPatchParam(gl_PrimitiveID);

	// Compute per-patch vertices.

	vec3 pos2[16];
	OsdPerPatchVertexBezier pos3[16];

	for (int i = 0; i < 16; i++)
		pos2[i] = pos[i];

	OsdComputePerPatchVertexBSpline(patchParam, gl_InvocationID, pos2, v[gl_InvocationID]);

	// Compute tessellation factors.
	if (gl_InvocationID == 0) {
		vec4 tessLevelOuter = vec4(0);
		vec2 tessLevelInner = vec2(0);

        OsdComputePerPatchVertexBSpline(patchParam,  0, pos2, pos3[0]);
        OsdComputePerPatchVertexBSpline(patchParam,  3, pos2, pos3[1]);
        OsdComputePerPatchVertexBSpline(patchParam, 15, pos2, pos3[2]);
        OsdComputePerPatchVertexBSpline(patchParam, 12, pos2, pos3[3]);

        l[gl_InvocationID].x = distance(pos3[0].P.xyz, pos3[1].P.xyz);
        l[gl_InvocationID].y = distance(pos3[1].P.xyz, pos3[2].P.xyz);
        l[gl_InvocationID].z = distance(pos3[2].P.xyz, pos3[3].P.xyz);
        l[gl_InvocationID].w = distance(pos3[3].P.xyz, pos3[0].P.xyz);


        vec2 uv = 0.25*(uv_in[5].uv + uv_in[6].uv + uv_in[9].uv + uv_in[10].uv);

		OsdGetTessLevelsUniform(patchParam,
								tessLevelOuter, tessLevelInner,
								tessOuterLo[0], tessOuterHi[0]);

        int transitionMask;

        gl_TessLevelOuter[0] = tessLevelOuter[0]*3;
        gl_TessLevelOuter[1] = tessLevelOuter[1]*3;
        gl_TessLevelOuter[2] = tessLevelOuter[2]*3;
        gl_TessLevelOuter[3] = tessLevelOuter[3]*3;

		gl_TessLevelInner[0] = tessLevelInner[0]*3;
		gl_TessLevelInner[1] = tessLevelInner[1]*3;
	}
    uv_out[gl_InvocationID].uv = uv_in[gl_InvocationID].uv;
}
#endif


//////////////////////////////////////////
#ifdef TESS_EVALUATION_SHADER

layout(quads) in;
patch in vec4 tessOuterLo[1], tessOuterHi[1];
in OsdPerPatchVertexBezier v[];

in vec4 l[];

uniform float midStrength;
uniform float strength;

uniform sampler2D displaceTexture;
uniform sampler2D displaceTexture2;
uniform float t;

in UV_DATA_TES
{
    vec2 uv;
}uv_in[];

out UV_DATA_GEO
{
    vec2 uv;
}uv_out;

out vec3 norm;


void interpolateUV(in vec2 UV, out vec2 uv)
//vstup UV - koordinat patchu
//vystup uv - koordinat textury
{
    vec2 uv1, uv2;

    uv1 = mix(uv_in[ 5].uv, uv_in[ 6].uv, UV.x);
    uv2 = mix(uv_in[ 9].uv, uv_in[10].uv, UV.x);

    uv = mix(uv1, uv2, UV.y);
}


vec4 getTextureValue(vec2 uv)
{
    vec4 val1 = texture(displaceTexture, uv);
    vec4 val2 = texture(displaceTexture2, uv);

    return mix(val1, val2, t);
}

void main()
{
	// Compute tesscoord.
	float theta = 0.2;

	vec2 UV = OsdGetTessParameterization(gl_TessCoord.xy, tessOuterLo[0], tessOuterHi[0]), UV2, UV3;

    ivec3 patchParam = v[0].patchParam;
	int refinementLevel = OsdGetPatchRefinementLevel(patchParam);
    theta *= pow(2, refinementLevel-1);

    UV2 = UV + vec2(theta, 0);
    UV3 = UV + vec2(0, theta);
    vec2 uv2, uv3;
	vec3 P = vec3(0), dPu = vec3(0), dPv = vec3(0);
	vec3 N = vec3(0), dNu = vec3(0), dNv = vec3(0);

    vec3 P2 = vec3(0), dPu2 = vec3(0), dPv2 = vec3(0);
	vec3 N2 = vec3(0), dNu2 = vec3(0), dNv2 = vec3(0);

    vec3 P3 = vec3(0), dPu3 = vec3(0), dPv3 = vec3(0);
	vec3 N3 = vec3(0), dNu3 = vec3(0), dNv3 = vec3(0);




	OsdPerPatchVertexBezier v2[16];

	for (int i = 0; i < 16; i++)
		v2[i] = v[i];

	// Evaluate patch at the tess coord UV
	OsdEvalPatchBezier(patchParam, UV, v2, P , dPu , dPv , N , dNu , dNv );
	OsdEvalPatchBezier(patchParam, UV2, v2, P2, dPu2, dPv2, N2, dNu2, dNv2);
	OsdEvalPatchBezier(patchParam, UV3, v2, P3, dPu3, dPv3, N3, dNu3, dNv3);


	// Apply model-view-projection matrix.

    interpolateUV(UV,  uv_out.uv);
    interpolateUV(UV2, uv2);
    interpolateUV(UV3, uv3);

    vec3 dPos  = strength*(getTextureValue(uv_out.uv).xyz - midStrength);
    vec3 dPos2 = strength*(getTextureValue(      uv2).xyz - midStrength);
    vec3 dPos3 = strength*(getTextureValue(      uv3).xyz - midStrength);


    mat3 patchMatrix  = mat3(normalize(dPu ), normalize(dPv ), normalize(N ));
    mat3 patchMatrix2 = mat3(normalize(dPu2), normalize(dPv2), normalize(N2));
    mat3 patchMatrix3 = mat3(normalize(dPu3), normalize(dPv3), normalize(N3));

    vec3 dPosG  = P  + patchMatrix *dPos;
    vec3 dPosG2 = P2 + patchMatrix2*dPos2;
    vec3 dPosG3 = P3 + patchMatrix3*dPos3;

    vec3 ab = dPosG2 - dPosG;
    vec3 ac = dPosG3 - dPosG;

    N2 = normalize(cross(ab, ac));

    N3.x = dot(N2, patchMatrix[0]);
    N3.y = dot(N2, patchMatrix[1]);
    N3.z = dot(N2, patchMatrix[2]);

	gl_Position = vec4(2*uv_out.uv - 1, 0, 1);

	norm = N3;
}
#endif
/////////////////////////////////////
#ifdef GEOMETRY_SHADER

in UV_DATA_GEO
{
    vec2 uv;
}uv_in[];



out vec3 norm;

layout (triangles) in;
layout (triangle_strip) out;
layout (max_vertices = 3) out;

out vec3 deb;

void main()
{
    int i;

    for (i = 0; i < gl_in.length(); i++)
    {
        gl_Position = vec4(2*uv_in[i].uv - 1, 0.0, 1.0);
        EmitVertex();
    }
    EndPrimitive();
}

#endif // GEOMETRY_SHADER

///////////////////////////////////////////
#ifdef FRAGMENT_SHADER

in vec3 pos;
out vec4 color;

in vec3 norm;

in vec3 deb;


in TANGENT
{
    vec3 N;
    vec3 dPu;
    vec3 dPv;
}tan_in[];

void main()
{
	//color = vec4(0.5*deb + 0.5, 1.0);

    vec3 normalLocal = normalize(norm);
	color = vec4(0.5*normalize(normalLocal) + 0.5, 1);
	//color = vec4(1.0, 0.0, 0.0, 1.0);
}
#endif

////old
    /*int x = max(min(int(3*UV.x), 2), 0);
    int y = max(min(int(3*UV.y), 2), 0);

    vec2 duv, uv1, uv2;

    duv.x = 3*(UV.x - float(x)/3);
    duv.y = 3*(UV.y - float(y)/3);

    ivec4 patchIndex = ivec4(x + 4*y, x + 4*y + 1, x + 4*y + 4, x + 4*y + 5);

    uv1 = mix(uv_in[patchIndex[0]].uv, uv_in[patchIndex[1]].uv, duv.x);
    uv2 = mix(uv_in[patchIndex[2]].uv, uv_in[patchIndex[3]].uv, duv.x);

    uv = mix(uv1, uv2, duv.y);
    uv = uv_in[5].uv;*/

