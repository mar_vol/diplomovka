#ifndef _UNIFORM_H_
#define _UNIFORM_H_

#ifndef NULL
#define NULL 0
#endif

typedef struct iUniform{
    int loc;
    char name[32];
    int value;
    int program;

    void update(const int newValue);
    iUniform(int program, const char* name, int value = 0);

} I_UNIFORM;

typedef struct fUniform{
    int loc;
    char name[32];
    float value;
    int program;

    void update(const float newValue);
    fUniform(int program, const char* name, const float value = 0.0f);

} F_UNIFORM;

typedef struct vec3Uniform{
    int loc;
    char name[32];
    float value[3];
    int program;

    void update(const float* newValue);
    vec3Uniform(int program, const char* name, const float* value = NULL);

} VEC3_UNIFORM;

typedef struct vec2Uniform{
    int loc;
    char name[32];
    float value[2];
    int program;

    void update(const float* newValue);
    vec2Uniform(int program, const char* name, const float* value = NULL);

} VEC2_UNIFORM;

typedef struct mat4Uniform{
    int loc;
    char name[32];
    float value[16];
    int program;

    void update(const float* newValue);
    mat4Uniform(int program, const char* name, const float* value = NULL);
} MAT_4_UNIFORM;

typedef struct program
{
    int programID;
    static int actualProgram;
    program(int programID);
    ~program();
    void use();
    static int getActualProgram();
    static void setActualProgram(int programID);
}PROGRAM;

#endif // _UNIFORM_H_
