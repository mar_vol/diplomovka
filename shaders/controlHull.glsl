#ifdef VERTEX_SHADER

in layout(location = 0) vec3 pos;

uniform mat4 MVP;

void main(void)
{
    gl_Position = MVP*vec4(pos, 1.0);

}
#endif

#ifdef FRAGMENT_SHADER

uniform bool line;
uniform vec3 color;

out vec4 colorOut;
uniform sampler2D tex;
uniform int selectedVertex;

void main()
{
    if (line)
        colorOut = vec4(0.2, 0.2, 0.2, 1.0);
    else
        if (gl_PrimitiveID == selectedVertex)
            colorOut = vec4(1.0, 0.0, 0.0, 1.0);
        else
             colorOut = vec4(0.0, 1.0, 1.0, 1.0);
}
#endif
