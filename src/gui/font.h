#ifndef _FONT_H_
#define _FONT_H_

#define GLEW_STATIC
#include <GL/glew.h>
#include "../shader/uniform.h"

class Font
{
    public:
        Font(const char* fontFile, PROGRAM* fontProgram);
        ~Font();
        void renderText(const unsigned char* text, float x, float y, float height, float aspect);
    private:
        struct CHAR
        {
            float u, v; //uv - koordinat
            float u2, v2; //sirka
            float x, y; //offset
        }charBuffer[256];
        GLuint posTexture;
        GLuint fontTexture;
        int lastSize;

        float xadvanceBuffer[256];
        float lineHeight;
        float base;

        VEC2_UNIFORM fStart;
        F_UNIFORM fBase;
        F_UNIFORM fHeight;
        F_UNIFORM fAspect;
        I_UNIFORM posT;

        GLuint VAO;
        GLuint VBO;
        GLuint EBO;
        PROGRAM* fontProgram;
        I_UNIFORM* fontText;

};

#pragma pack(push, 1) //BMP hlavicka
typedef struct bmp
{
    uint16_t Mn;
    uint32_t Size, Application, Offset, DIB, Width, Height;
    uint16_t Planes, BitsPerPixels;
    uint32_t BI_RGB1, Raw, HorizontalResolution, Verticalresolution, Colors,  Mean;
} BMP;
#pragma pack(pop)

int generateTexture(const char* const file, bool mipmap = true, int nullW = 0, int nullH = 0);
unsigned char* loadBMP(const char* const f, int* w, int* h, int* mode);

#endif
