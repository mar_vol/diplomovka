#include "object.h"
#include "../shader/matrix.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>



//FAR

#include <opensubdiv/far/primvarRefiner.h>

//OSD
#include <opensubdiv/osd/cpuEvaluator.h>
#include <opensubdiv/osd/cpuGLVertexBuffer.h>

#include <opensubdiv/osd/glLegacyGregoryPatchTable.h>
#include <opensubdiv/osd/glXFBEvaluator.h>
#include <opensubdiv/osd/glVertexBuffer.h>
#include <opensubdiv/osd/glComputeEvaluator.h>


#include <opensubdiv/osd/cpuPatchTable.h>

void UV_Vertex::Clear()
{
    u=0.0;
    v=0.0;
}

void UV_Vertex::AddWithWeight(UV_Vertex const & src, float weight)
{
    u += weight * src.u;
    v += weight * src.v;
}


Object::Object(const char* file, PROGRAM* program, PROGRAM* normalProgram, PROGRAM* controlHullProgram, int subdivisionLevel, bool CPU)
{
    initialized = false;
    if (loadObjFromFile(file))
    {
        this->subdivisionLevel = subdivisionLevel;
        this->CPU = CPU;
        this->program = program;
        this->normalProgram = normalProgram;
        this->controlHullProgram = controlHullProgram;
        //printf("pos: %d, index: %d, uv: %d uv2: %d num: %d\n", pos.size(), index.size(), uv.size(), fvBufferUV.size(), numVerticesPerFace.size());
        initOpenSubdiv();
        initOpenGL();
    }
}

Object::~Object()
{
    if (initialized)
    {
        deallocate();
    }

}

void Object::deallocate()
{
    pos.clear();
    uv.clear();
    index.clear();
    lines.clear();
    delete mesh;
}

int getline(char* line, int* len, FILE* f)
{
    int ret = -1;
    int i = 0, c;
    *len = -1;

    while (true)
    {
        c = fgetc(f);
        if (c == '\n' || c == EOF)
        {
            line[i] = '\0';
            *len = i;
            if (c != EOF)
                return 1;
            else
                return -1;
        }


        if (c == '\r')
            continue;

        line[i++] = (char)c;
    }
    //printf("c: %d %d\n", c, ftell(f));
    return ret;
}

int Object::loadObjFromFile(const char* file)
{
    FILE *f = fopen(file, "rb");
    char line[256];
    char type[8];
    char temp[4][16];
    int topology, i, len, read, curPos, curUv, first, last;

    float vec3[3] = {0.0, 0.0, 0.0};
    float vec2[2] = {0.0, 0.0};

    std::vector<float> uvTemp;
    std::vector<int> uvITemp;

    if (f == NULL)
    {
        fprintf(stderr, "Nemozem otvorit subor: %s (%s)\n", file, __FUNCTION__);
        getchar();
        return 0;
    }

    if (initialized)
    {
        deallocate();
    }

    numVertices = 0;
    numUV = 0;
    numFaces = 0;

    do
    {
        read = getline(line, &len, f);
        if (read == -1  && len <= 0)
            break;
        sscanf(line, "%s", type);
        //printf("line: %s (%s %d | %d) %X\n", line, type, len, strlen(line), ftell(f));
        if (!strcmp(type, "v"))
        {
            memset(vec3, 0, sizeof(vec3));
            sscanf(line, "%s%f%f%f", type, vec3, vec3 + 1, vec3 + 2);
            //printf("POS!!! %.3f %.3f %.3f\n", vec3[0], vec3[1], vec3[2]);

            pos.push_back(vec3[0]);
            pos.push_back(vec3[1]);
            pos.push_back(vec3[2]);
            numVertices++;
        }
        else if (!strcmp(type, "vt"))
        {
            memset(vec2, 0, sizeof(vec2));
            sscanf(line, "%s%f%f", type, vec2, vec2 + 1);
            //printf("UV!!! %.3f %.3f\n", vec2[0], vec2[1]);

            uvTemp.push_back(vec2[0]);
            uvTemp.push_back(vec2[1]);
        }
        else if (!strcmp(type, "f"))
        {
            topology = sscanf(line, "%s%s%s%s%s", type, temp[0], temp[1], temp[2], temp[3]) - 1;

            //printf("topology: %d, line: %s\n", topology, line);

            for (i = 0; i < topology; i++)
            {
                sscanf(temp[i], "%d/%d", &curPos, &curUv);

                curPos--;
                curUv--;

                if (i > 0)
                {
                    lines.push_back(index.back());
                    lines.push_back(curPos);
                    last = curPos;
                }
                else
                {
                    first = curPos;
                }

                //printf("\t%d %d\n", curPos, curUv);
                if (2*curPos + 1 >= (int) uv.size())
                {
                    uv.resize(2*2*curPos + 2);
                    uvITemp.resize(2*curPos + 1);
                    //printf("resize: %d\n", 2*2*curPos);
                }

                //printf("curPos: %d, curUv: %d uv0: %.3f uv1: %.3f uvt0: %.3f uvt1: %.3f\n", curPos, curUv, uv[2*curPos], uv[2*curPos + 1], uvTemp[2*curUv], uvTemp[2*curUv + 1]);
                uv[2*curPos]     += uvTemp[2*curUv];
                uv[2*curPos + 1] += uvTemp[2*curUv + 1];
                uvITemp[curPos]++;

                //printf("curPos: %d, curUv: %d uv0: %.3f uv1: %.3f uvt0: %.3f uvt1: %.3f\n",  curPos, curUv, uv[2*curPos], uv[2*curPos + 1], uvTemp[2*curUv], uvTemp[2*curUv + 1]);
                //getchar();
                index.push_back(curPos);
            }
            if (topology > 2)
            {
                lines.push_back(last);
                lines.push_back(first);
            }
            numVerticesPerFace.push_back(topology);

            //printf("topology: %d!!!\n", topology);
            numFaces++;
        }
    }
    while (read != -1);

    /*for (unsigned int i = 0; i < lines.size(); i++)
    {
        printf("%d: %d\n", i, lines[i]);
    }*/

    for (unsigned int i = 0; i < uv.size(); i++)
    {
        if (uvITemp[i/2])
            uv[i] /= uvITemp[i/2];
    }

    fclose(f);
    numUV = numVertices;
    uv.resize(2*numVertices);
    initialized = true;
    return 1;
}

using namespace OpenSubdiv;

void Object::initOpenSubdiv()
{
    printf("init opensbdv\n");

    glGenVertexArrays(1, &VAOupdate);
    glBindVertexArray(VAOupdate);

    Far::TopologyDescriptor::FVarChannel uvChannel;
    uvChannel.numValues  = numVertices;
    uvChannel.valueIndices = (const int*) &index[0];

    //nastavenie topologie
    topologyDescriptor.numVertices        = numVertices;
    topologyDescriptor.numFaces           = numFaces;
    topologyDescriptor.numVertsPerFace    = &numVerticesPerFace[0];
    topologyDescriptor.vertIndicesPerFace = (const int*) &index[0];

    printf("numVertices: %d, numFaces: %d\n", numVertices, numFaces);

    //pridanie uv koordinatov ako FVar channel
    topologyDescriptor.numFVarChannels = 1;
    topologyDescriptor.fvarChannels = &uvChannel;

    Sdc::SchemeType type = OpenSubdiv::Sdc::SCHEME_CATMARK;

    Sdc::Options options;
    options.SetVtxBoundaryInterpolation(Sdc::Options::VTX_BOUNDARY_EDGE_ONLY);


    //Vytvorenie topology refiner
    topologyRefiner = Far::TopologyRefinerFactory<Far::TopologyDescriptor>::Create
                      (topologyDescriptor, Far::TopologyRefinerFactory<Far::TopologyDescriptor>::Options(type, options));

    //Pridanie prvych uv do pola fvBufferUV, ktore sa postupnym aplikovanym subdived, bude zvacsovat
    fvBufferUV.resize(topologyRefiner->GetNumFVarValuesTotal(0));
    UV_Vertex * uvVertex = &fvBufferUV[0];

    for (int i = 0; i < topologyDescriptor.numVertices; ++i)
    {
        uvVertex[i].u = uv[2*i];
        uvVertex[i].v = uv[2*i + 1];
        //printf("uv: %.3f %.3f\n", uvVertex[i].u, uvVertex[i].v);
    }

    //2. Setup Osd::Mesh. In this example we use b-spline endcap.

    Osd::MeshBitset bits;
    bits.set(Osd::MeshAdaptive, true);
    bits.set(Osd::MeshEndCapBSplineBasis, true);

    if (CPU)
    {
        //printf("nv: %d\n", topologyRefiner->GetNumFVarValuesTotal(0));
        mesh = new Osd::Mesh<Osd::CpuGLVertexBuffer,
        Far::StencilTable,
        Osd::CpuEvaluator,
        Osd::GLPatchTable>
        (topologyRefiner, numVertexElements, 2, subdivisionLevel, bits);
        //printf("nv: %d\n", topologyRefiner->GetNumFVarValuesTotal(0));
    }
    else
    {
        mesh = new Osd::Mesh<Osd::GLVertexBuffer,
        Osd::GLStencilTableSSBO,
        Osd::GLComputeEvaluator,
        Osd::GLPatchTable>
        (topologyRefiner, numVertexElements, 2, subdivisionLevel, bits);

    }

    printf("AAAA\n");
    update();
    glBindVertexArray(0);

    glGenVertexArrays(1, &VAOcontrol);
    glBindVertexArray(VAOcontrol);
    printf("VAO weqwf\n");
        glGenBuffers(1, &VBOcontrol);
        glBindBuffer(GL_ARRAY_BUFFER, VBOcontrol);
        glBufferData(GL_ARRAY_BUFFER, pos.size()*sizeof(pos[0]), &pos[0], GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, FALSE, 0, (GLvoid*) NULL);

        glGenBuffers(1, &EBOcontrol);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBOcontrol);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, lines.size()*sizeof(lines[0]), &lines[0], GL_STATIC_DRAW);


    glBindVertexArray(0);
    printf("BBBB\n");
}

void Object::update()
{
    //glBindVertexArray(VAOupdate);
    printf("aaaa\n");
    glBindVertexArray(VAO);
        mesh->UpdateVertexBuffer(&pos[0], 0, topologyDescriptor.numVertices);
        mesh->UpdateVaryingBuffer((float*) &fvBufferUV[0], 0, topologyDescriptor.numVertices);
        mesh->Refine();
    glBindVertexArray(0);
}

void Object::printIndexBuffer()
{
    printf("--------Index buffer:-------\n");
    for (unsigned int i = 0; i < index.size(); i++)
        printf("%d ", index[i]);

    printf("----------------------------\n");
}

void Object::printUvBuffer()
{
    printf("--------UV buffer:-------\n");
    for (unsigned int i = 0; i < uv.size(); i+=2)
    {
        printf("%d: %.3f %.3f\n", i/2, uv[i], uv[i+1]);

    }
    printf("----------------------------\n");
}

void Object::printPosBuffer()
{
    printf("--------Pos buffer:-------\n");
    for (unsigned int i = 0; i < pos.size(); i+=3)
    {
        printf("%d: %.3f %.3f %.3f\n", i/3, pos[i], pos[i+1], pos[i+2]);

    }
    printf("----------------------------\n");
}

void Object::printFaceBuffer()
{
    printf("--------Face buffer:-------\n");
    for (unsigned int i = 0; i < numVerticesPerFace.size(); i++)
    {
        printf("%d: %d\n", i, numVerticesPerFace[i]);

    }
    printf("----------------------------\n");
}

void Object::initOpenGL()
{
    printf("init OpenGL\n");

    program->use();

    int posLocation = glGetAttribLocation(program->programID, "position");
    int uvLocation  = glGetAttribLocation(program->programID, "uv");

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);


        glBindBuffer(GL_ARRAY_BUFFER, mesh->BindVertexBuffer());
        glVertexAttribPointer(posLocation, numVertexElements, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(posLocation);

        glGenBuffers(1, &uvBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, mesh->BindVaryingBuffer());
        glVertexAttribPointer(uvLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(uvLocation);
    glBindVertexArray(0);
}

void Object::render(bool normalMap, const char* outputFile)
{
    if (normalMap)
        normalProgram->use();
    else
        program->use();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_BUFFER, mesh->GetPatchTable()->GetPatchParamTextureBuffer());

    glBindVertexArray(VAO);

    Osd::PatchArray const & patch = mesh->GetPatchTable()->GetPatchArrays()[0];
    Far::PatchDescriptor desc = patch.GetDescriptor();

    int numVertsPerPatch = desc.GetNumControlVertices();  // 16 for B-spline patches
    //printf("patch: %d desc type: %d\n", numVertsPerPatch, desc.GetType());

    int patch_index = 5;

    glUniform1i(glGetUniformLocation(program->programID, "PrimitiveIdBase"), patch_index);
    glUniform1i(glGetUniformLocation(program->programID, "osd_fvar_count"), 0);


    int nBufferSize = 0;

    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &nBufferSize);
    glPatchParameteri(GL_PATCH_VERTICES, numVertsPerPatch);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->GetPatchTable()->GetPatchIndexBuffer());
    glDrawElements(GL_PATCHES, patch.GetNumPatches() * numVertsPerPatch, GL_UNSIGNED_INT, 0);

    if (outputFile != NULL)
        saveToFile(outputFile);

    glBindVertexArray(0);

}

void Object::renderControlHull(I_UNIFORM* line)
{
    controlHullProgram->use();
    glBindVertexArray(VAOcontrol);

    line->update(1);
    glDrawElements(GL_LINES, lines.size(), GL_UNSIGNED_INT, 0);
    line->update(0);
    glDrawArrays(GL_POINTS, 0, pos.size()/3);
}

int Object::getNearestVertex(float xNDC, float yNDC, const float* MVP, float radius)
{
    float pom[4];
    float minDistance = 1000, distance;
    int index = -1;

    for (unsigned int i = 0; i < pos.size(); i+=3)
    {
        getPointW(pom, &pos[i], MVP);
        pom[0] /= pom[3];
        pom[1] /= pom[3];

        distance = sqrt((pom[0] - xNDC)*(pom[0] - xNDC) + (pom[1] - yNDC)*(pom[1] - yNDC));

        if (i == 0)
        {
            minDistance = distance;
            index = 0;
        }

        else if (distance < minDistance)
        {
            minDistance = distance;
            index = i/3;
        }
    }

    if (minDistance <= radius)
    {
        return index;
    }


    return -1;
}

void Object::startMovingVertex(int id, float xNDC, float yNDC, const float* MVP)
{
    if (id < 0 || id >= (int) pos.size()/3)
        return;

    invertMatrix(MVP, mState.iMatrix);
    getPointW(mState.startPos, &pos[3*id], MVP);
    mState.xNDC = xNDC;
    mState.yNDC = yNDC;
    mState.id = id;
    mState.moving = true;
}

void Object::cancelMovingVertex()
{
    mState.moving = false;
}

void Object::moveVertex(float xNDC, float yNDC)
{
    float pom[4], newPosition[4];

    if (mState.moving && (mState.id >= 0 || mState.id < (int) pos.size()/3))
    {
        pom[0] = mState.startPos[0] + mState.startPos[3]*(xNDC - mState.xNDC);
        pom[1] = mState.startPos[1] + mState.startPos[3]*(yNDC - mState.yNDC);
        pom[2] = mState.startPos[2];
        pom[3] = mState.startPos[3];

        getPointW4(newPosition, pom, mState.iMatrix);
        memcpy(&pos[3*mState.id], newPosition, 3*sizeof(float));

        glBindVertexArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, VBOcontrol);
        glBufferSubData(GL_ARRAY_BUFFER, 3*mState.id*sizeof(float), 3*sizeof(float), newPosition);

        update();
    }
}

void Object::saveToFile(const char* file)
{
    FILE *fout = fopen(file, "wb");
    if (fout == NULL)
        return;

    GLuint *index;
    GLint indexSize, vertSize, uvSize;
    GLfloat *vert, *uv;
    int i, j, u, v;

    glBindBuffer(GL_ARRAY_BUFFER, mesh->BindVertexBuffer());
    glGetBufferParameteriv(GL_ARRAY_BUFFER,         GL_BUFFER_SIZE, &vertSize);
    glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &indexSize);

    vert = (GLfloat*) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
    if (vert == NULL)
    {
        glUnmapBuffer(GL_ARRAY_BUFFER);
        fprintf(stderr, "Neda sa mapovat array buffer\n");
        getchar();
        return;
    }

    for (i = 0; i < (int) (vertSize/sizeof(float)); i+=3)
    {
        fprintf(fout, "v %.5f %.5f %.5f\n", vert[i], vert[i + 1], vert[i + 2]);
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->BindVaryingBuffer());
    glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &uvSize);
    uv = (GLfloat*)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
    if (uv == NULL)
    {
        glUnmapBuffer(GL_ARRAY_BUFFER);
        fprintf(stderr, "Neda sa mapovat array buffer\n");
        getchar();
        return;
    }

    for (i = 0; i < (int) (uvSize/sizeof(float)); i+=2)
    {
        fprintf(fout, "vt %.5f %.5f\n", uv[i], uv[i + 1]);
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);

    index = (GLuint*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_READ_ONLY);
    if (index == NULL)
    {
        glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
        fprintf(stderr, "Neda sa mapovat index buffer\n");
        getchar();
        return;
    }

    for (i = 0; i < (int) (indexSize/sizeof(GLuint)); i += 16)
    {
        fprintf(fout, "o %03d\n", i/16);
        for (j = 0; j < 9; j++)
        {
            u = j%3;
            v = j/3;
            fprintf(fout, "f %d/%d %d/%d %d/%d %d/%d\n", index[i + 4*v + u] + 1, index[i + 4*v + u] + 1,  index[i + 4*v + u + 1] + 1, index[i + 4*v + u + 1] + 1, index[i + 4*(v + 1) + u + 1] + 1, index[i + 4*(v + 1) + u + 1] + 1, index[i + 4*(v + 1) + u] + 1, index[i + 4*(v + 1) + u] + 1);
        }
    }
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    fclose(fout);
}
