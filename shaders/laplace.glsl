#ifdef VERTEX_SHADER

in layout(location = 0) vec3 pos;
in layout(location = 1) vec2 uv;

out vec2 Uv;

void main(void)
{
    gl_Position = vec4(pos, 1.0);
    Uv = uv;
}
#endif

#ifdef FRAGMENT_SHADER
uniform sampler2D inputTexture;
uniform float duv;

in vec2 Uv;

out vec4 color;

float laplace(in vec2 uv, in float duv, in sampler2D inputTexture, int color)
{
    float a, b, c, d, e, f = 0;

    if (uv.x - duv > 0)
    {
        a = texture(inputTexture, uv + vec2(   0, -duv))[color];
        f++;
    }
    else
        a = 0;

    if (uv.x + duv < 1)
    {
        b = texture(inputTexture, uv + vec2(   0,  duv))[color];
        f++;
    }
    else
        b = 0;

    if (uv.y - duv > 0)
    {
        c = texture(inputTexture, uv + vec2(-duv,    0))[color];
        f++;
    }
    else
        c = 0;

    if (uv.y + duv < 1)
    {
        d = texture(inputTexture, uv + vec2( duv,    0))[color];
        f++;
    }
    else
        d = 0;

    e = texture(inputTexture, uv)                   [color];

    return abs(a + b + c + d - f*e);
}

float max3(in float a, in float b, in float c)
{
    return max(max(a, b), c);
}

float min3(in float a, in float b, in float c)
{
    return min(min(a, b), c);
}

void main()
{
    float r, g, b, R, G, B;
    r = laplace(Uv, duv, inputTexture, 0)*10;
    g = laplace(Uv, duv, inputTexture, 1)*10;
    b = laplace(Uv, duv, inputTexture, 2)*10;

    R = max3(r, g, b);
    G = min3(r, g, b);
    B = (r + g + b)/3;

    color = vec4(R, G, B, 1.0);
}

#endif
