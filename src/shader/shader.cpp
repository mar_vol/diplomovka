#include "shader.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

GLuint loadShaders(const char* filePath, bool tesselation, bool geometry, bool feedback)
{
    GLuint programID = glCreateProgram();
    // Create the shaders
    GLuint vertexShaderID           = attachShader(programID, GL_VERTEX_SHADER,          filePath);
    GLuint tessControlShaderID;
    GLuint tessEvaluationShaderID;
    GLuint geometryShaderID;
    if (tesselation)
    {
        tessControlShaderID      = attachShader(programID, GL_TESS_CONTROL_SHADER,    filePath, "shaders/glslPatchCommon.glsl");
        tessEvaluationShaderID   = attachShader(programID, GL_TESS_EVALUATION_SHADER, filePath, "shaders/glslPatchCommon.glsl");

    }
    if (geometry)
        geometryShaderID         = attachShader(programID, GL_GEOMETRY_SHADER,        filePath);

    GLuint fragmentShaderID      = attachShader(programID, GL_FRAGMENT_SHADER,        filePath);

#ifdef GL_DEBUG
    static const char * tf_varyings[] =
    {
        "feedback_position",
        "feedback_normal",
        "feedback_uv"
    };

    if (feedback == true)
    {
        glTransformFeedbackVaryings(programID, sizeof(tf_varyings)/sizeof(tf_varyings[0]), tf_varyings, GL_INTERLEAVED_ATTRIBS);
        printf("Transform error: %d\n", glGetError());
        getchar();
    }

#endif

    // Link the program
    printf("Linking program err: %d\n", glGetError());
    glLinkProgram(programID);
    GLint infoLogLength;
    GLint result;

    // Check the program
    glGetProgramiv(programID, GL_LINK_STATUS, &result);
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if ( infoLogLength > 0 )
    {
        char* errorMessage = (char*)malloc(sizeof(char)* (infoLogLength+1));
        glGetProgramInfoLog(programID, infoLogLength, NULL, &errorMessage[0]);
        printf("aaa: %s\n", errorMessage);
        free(errorMessage);

    }

    if (result == GL_FALSE)
        getchar();

    glDeleteShader(vertexShaderID);
    if (tesselation)
    {
        glDeleteShader(tessControlShaderID);
        glDeleteShader(tessEvaluationShaderID);
    }
    if (geometry)
        glDeleteShader(geometryShaderID);

    glDeleteShader(fragmentShaderID);

    printf("PROGRAM: %d :%s\n", programID, filePath);
    return programID;
}


GLuint attachShader(const GLuint programID, const GLuint type, const char* filePath, const char* tessellationShaderCommonPath)
{
    GLuint shaderID = glCreateShader(type);
    FILE *file = fopen(filePath, "rb");

    char options[256];
    char *wholeProgram[2];

    if (file == NULL)
    {
        fprintf(stderr, "Nemozem najst: %s\n", filePath);
        return 0;
    }

    fseek(file, 0L, SEEK_END);
    unsigned int fileSize = ftell(file);
    rewind(file);

    char* shaderString = (char*) malloc(sizeof(char) * (fileSize + 1));
    fread(shaderString, 1, fileSize, file);
    shaderString[fileSize] = '\0';
    fclose(file);

    wholeProgram[0] = options;
    wholeProgram[1] = shaderString;

    switch(type)
    {
    case GL_VERTEX_SHADER:
        strcpy(options, "#version 430 core\n#define VERTEX_SHADER\n\n");
        break;
    case GL_TESS_CONTROL_SHADER:
        strcpy(options, "#version 430 core\n#define TESS_CONTROL_SHADER\n#define OSD_FVAR_WIDTH 2\n\n");
        break;
    case GL_TESS_EVALUATION_SHADER:
        strcpy(options, "#version 430 core\n#define TESS_EVALUATION_SHADER\n#define OSD_FVAR_WIDTH 2\n\n");
        break;
    case GL_GEOMETRY_SHADER:
#ifdef WIREFRAME
        strcpy(options, "#version 430 core\n#define GEOMETRY_SHADER\n#define WIREFRAME\n\n");
#else
        strcpy(options, "#version 430 core\n#define GEOMETRY_SHADER\n\n");
#endif
        break;
    case GL_FRAGMENT_SHADER:
        strcpy(options, "#version 430 core\n#define FRAGMENT_SHADER\n\n");
        break;
    default:
        fprintf(stderr, "Neznamy typ shadera: %d\n", type);
        return 0;
    }

    if (tessellationShaderCommonPath == NULL)
        printf("NULL");

    if (tessellationShaderCommonPath == NULL || (type != GL_TESS_CONTROL_SHADER && type != GL_TESS_EVALUATION_SHADER))
    {
        glShaderSource(shaderID, sizeof(wholeProgram)/sizeof(char*), wholeProgram , NULL);
        glCompileShader(shaderID);
    }
    else
    {
        char *wholeProgram2[3];
        file = fopen(tessellationShaderCommonPath, "rb");
        if (file == NULL)
        {
            fprintf(stderr, "Nemozem najst: %s\n", tessellationShaderCommonPath);
            return 0;
        }

        fseek(file, 0L, SEEK_END);
        unsigned int fileSize = ftell(file);
        rewind(file);

        char* tessellationShaderCommonString = (char*) malloc(sizeof(char) * (fileSize + 1));
        fread(tessellationShaderCommonString, 1, fileSize, file);
        tessellationShaderCommonString[fileSize] = '\0';
        fclose(file);

        wholeProgram2[0] = wholeProgram[0];
        wholeProgram2[1] = tessellationShaderCommonString;
        wholeProgram2[2] = wholeProgram[1];

        glShaderSource(shaderID, sizeof(wholeProgram2)/sizeof(char*), wholeProgram2, NULL);
        glCompileShader(shaderID);

        free(tessellationShaderCommonString);
    }

    GLint infoLogLength;
    GLint result;
    // Check Vertex Shader
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
    if ( infoLogLength > 0 )
    {
        char* errorMessage = (char*)malloc(sizeof(char)* (infoLogLength+1));
        glGetShaderInfoLog(shaderID, infoLogLength, NULL, &errorMessage[0]);
        fprintf(stderr, "%s: %s\n", options, errorMessage);
        free(errorMessage);
    }

    glAttachShader(programID, shaderID);
    return shaderID;
}

