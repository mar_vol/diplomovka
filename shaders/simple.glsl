
#ifdef VERTEX_SHADER

in layout(location = 0) vec3 pos;
in layout(location = 1) vec3 norm;
in layout(location = 2) vec2 uv;

out vec3 Norm;
out vec2 UV;

uniform mat4 MVP;

void main(void)
{
    gl_Position = MVP*vec4(pos, 1.0);
    Norm = norm;
    UV = uv;
}
#endif

#ifdef FRAGMENT_SHADER

in vec3 Norm;
in vec2 UV;

out vec4 color;
uniform sampler2D tex;

void main()
{
    vec3 norm = normalize(Norm);
    vec4 text = texture(tex, UV);

    color = color = vec4(0.8*text.r, 0.8*text.g, 0.8*text.b, 1.0)*norm.z;
}
#endif
