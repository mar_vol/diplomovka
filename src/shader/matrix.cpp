#include "matrix.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#define RAD_RATIO 0.017453292519943295


float modelViewMatrix[16];
float projectionMatrix[16];

void printMatrix(const float *m, const int n)
{
     //vypise matice, n - vyska a sirka matice
     int i, j;
     for (i = 0; i < n; i++)
     {
         for (j = 0; j < n; j++)
             printf("%.5f\t", m[i + j*n]);
         putchar('\n');
     }
     putchar('\n');
}

void multipleMatrix4x4 (float *m_out, const float* m1, const float* m2)
{
     //vynasobi maticu m1 a m2 (m1 je pred m2) a vysledok zapise do m_out
     int i , j;
     for (i = 0; i < 16; i++)
     {
         m_out[i] = 0;
         for (j = 0; j < 4; j++)
             m_out[i] +=  m1[(i&3)+(j<<2)] * m2[(i&12) + j];
     }
}

void copyMatrix(float *m_out, const float *m_in, const int n)
{
     //skopiruje maticu m_in do m_out, n udava pocet prvkov v matici (WIDTH * HEIGHT)
     int i;
     for (i = 0; i < n; i++)
         m_out[i] = m_in[i];
}

void translate(const float g_x, const float g_y, const float g_z, float *m)
{
    //posun v 3D priestore
    float translate_matrix[16] = {
                                     1.0, 0.0, 0.0, 0.0,
                                     0.0, 1.0, 0.0, 0.0,
                                     0.0, 0.0, 1.0, 0.0,
                                     g_x, g_y, g_z, 1.0
                                 },
    temp_matrix[16];
    //matica sa skopiruje, aby nevznikli side efekt pri nasobeni
    if (m == NULL)
        m = modelViewMatrix;
    copyMatrix(temp_matrix, m, 16);
    multipleMatrix4x4(m, temp_matrix, translate_matrix);
}

void scale(const float s_x, const float s_y, const float s_z, float *m)
{
    //zmena mierky 3D priestoru, zaporne znamienko preklopi os
    float scale_matrix[16] = {
                                     s_x, 0.0, 0.0, 0.0,
                                     0.0, s_y, 0.0, 0.0,
                                     0.0, 0.0, s_z, 0.0,
                                     0.0, 0.0, 0.0, 1.0
                                     },
    temp_matrix[16];

    if (m == NULL)
        m = modelViewMatrix;
    copyMatrix(temp_matrix, m, 16);
    multipleMatrix4x4(m, temp_matrix, scale_matrix);
}

void rotateX(const float a, float *m)
{
    //rotacia X, a je v uhloch
    float a2 = RAD_RATIO *a, c = cos(a2), s = sin(a2),
    rotate_matrix[16] = {
                                     1.0, 0.0, 0.0, 0.0,
                                     0.0,   c,   s, 0.0,
                                     0.0,  -s,   c, 0.0,
                                     0.0, 0.0, 0.0, 1.0
                                     },
    temp_matrix[16];

    if (m == NULL)
        m = modelViewMatrix;
    copyMatrix(temp_matrix, m, 16);
    multipleMatrix4x4(m, temp_matrix, rotate_matrix);
}

void rotateY(const float a, float *m)
{
    //rotacia y
    float a2 = RAD_RATIO *a, c = cos(a2), s = sin(a2),
    rotate_matrix[16] = {
                                       c, 0.0,  -s, 0.0,
                                     0.0, 1.0, 0.0, 0.0,
                                       s, 0.0,   c, 0.0,
                                     0.0, 0.0, 0.0, 1.0
                                     },
    temp_matrix[16];

    if (m == NULL)
        m = modelViewMatrix;
    copyMatrix(temp_matrix, m, 16);
    multipleMatrix4x4(m, temp_matrix, rotate_matrix);
}

void rotateZ(const float a, float *m)
{
    //rotacia z
    float a2 = RAD_RATIO *a, c = cos(a2), s = sin(a2),
    rotate_matrix[16] = {
                                       c,   s, 0.0, 0.0,
                                      -s,   c, 0.0, 0.0,
                                     0.0, 0.0, 1.0, 0.0,
                                     0.0, 0.0, 0.0, 1.0
                                     },
    temp_matrix[16];

    if (m == NULL)
        m = modelViewMatrix;
    copyMatrix(temp_matrix, m, 16);
    multipleMatrix4x4(m, temp_matrix, rotate_matrix);
}


void loadIdentity(float *m)
{
     //nacitanie jednotkovej matice do model view matrix
     int i;

    if (m == NULL)
        m = modelViewMatrix;

     for (i = 0; i < 16; i++)
         if (i%5)
            m[i] = 0.0;
         else //hlavna diagonala jednotky
             m[i] = 1.0;
}

void ortho(float left, float right, float top, float bottom, float nearVal, float farVal)
{
    float tx, ty, tz;

    tx = (left + right)/(left - right);
    ty = (bottom + top)/(bottom - top);
    tz = (nearVal + farVal)/(nearVal - farVal);

    projectionMatrix[ 0] = 2.0f/(right - left);
    projectionMatrix[ 1] = 0.0f;
    projectionMatrix[ 2] = 0.0f;
    projectionMatrix[ 3] = 0.0f;

    projectionMatrix[ 4] = 0.0f;
    projectionMatrix[ 5] = 2.0f/(top - bottom);
    projectionMatrix[ 6] = 0.0f;
    projectionMatrix[ 7] = 0.0f;

    projectionMatrix[ 8] = 0.0f;
    projectionMatrix[ 9] = 0.0f;
    projectionMatrix[10] = 2.0f/(nearVal - farVal);
    projectionMatrix[11] = 0.0f;

    projectionMatrix[12] = tx;
    projectionMatrix[13] = ty;
    projectionMatrix[14] = tz;
    projectionMatrix[15] = 1.0f;
}

void ortho(float left, float right, float top, float bottom)
{
    ortho(left, right, top, bottom, -1, 1);
}

void perspective(float fovy,  float aspect, float zNear, float zFar)
{
    float f = 1.0f/tan(0.5*RAD_RATIO*fovy);
    projectionMatrix[ 0] = f/aspect;
    projectionMatrix[ 1] = 0.0f;
    projectionMatrix[ 2] = 0.0f;
    projectionMatrix[ 3] = 0.0f;

    projectionMatrix[ 4] = 0.0f;
    projectionMatrix[ 5] = f;
    projectionMatrix[ 6] = 0.0f;
    projectionMatrix[ 7] = 0.0f;

    projectionMatrix[ 8] = 0.0f;
    projectionMatrix[ 9] = 0.0f;
    projectionMatrix[10] = (zNear + zFar)/(zNear - zFar);
    projectionMatrix[11] = -1.0f;

    projectionMatrix[12] = 0.0f;
    projectionMatrix[13] = 0.0f;
    projectionMatrix[14] = 2.0f*zNear*zFar/(zNear - zFar);
    projectionMatrix[15] = 0.0f;
}

void getProjectionMatrix(float *m)
{
    if (m != NULL)
        memcpy(m, projectionMatrix, sizeof(projectionMatrix));
}
void getModelViewMatrix(float *m)
{
    if (m != NULL)
        memcpy(m, modelViewMatrix, sizeof(projectionMatrix));
}

const float *getModelViewMatrix()
{
    return modelViewMatrix;
}
const float *getProjectionMatrix()
{
    return projectionMatrix;
}

const float *getModelViewProjectionMatrix()
{
    static float MVP[16];
    multipleMatrix4x4 (MVP, getProjectionMatrix(), getModelViewMatrix());
    return MVP;
}

void getPoint(float *p_out, const float *p_in, const float *m)
{
     //zapise do bodu p_out 3D suradnicu p_in, ktora bola transformovana maticou m
     p_out[0] = p_in[0]*m[0] + p_in[1]*m[4] + p_in[2]*m[ 8] + m[12];
     p_out[1] = p_in[0]*m[1] + p_in[1]*m[5] + p_in[2]*m[ 9] + m[13];
     p_out[2] = p_in[0]*m[2] + p_in[1]*m[6] + p_in[2]*m[10] + m[14];
}

void getPointW(float *p_out, const float *p_in, const float *m)
{
     p_out[0] = p_in[0]*m[0] + p_in[1]*m[4] + p_in[2]*m[ 8] + m[12];
     p_out[1] = p_in[0]*m[1] + p_in[1]*m[5] + p_in[2]*m[ 9] + m[13];
     p_out[2] = p_in[0]*m[2] + p_in[1]*m[6] + p_in[2]*m[10] + m[14];
     p_out[3] = p_in[0]*m[3] + p_in[1]*m[7] + p_in[2]*m[11] + m[15];
}

void getPointW4(float *p_out, const float *p_in, const float *m)
{
     p_out[0] = p_in[0]*m[0] + p_in[1]*m[4] + p_in[2]*m[ 8] + p_in[3]*m[12];
     p_out[1] = p_in[0]*m[1] + p_in[1]*m[5] + p_in[2]*m[ 9] + p_in[3]*m[13];
     p_out[2] = p_in[0]*m[2] + p_in[1]*m[6] + p_in[2]*m[10] + p_in[3]*m[14];
     p_out[3] = p_in[0]*m[3] + p_in[1]*m[7] + p_in[2]*m[11] + p_in[3]*m[15];
}

bool invertMatrix(const float m[16], float invOut[16])
{
    float inv[16], det;
    int i;

    inv[0] = m[5]  * m[10] * m[15] -
             m[5]  * m[11] * m[14] -
             m[9]  * m[6]  * m[15] +
             m[9]  * m[7]  * m[14] +
             m[13] * m[6]  * m[11] -
             m[13] * m[7]  * m[10];

    inv[4] = -m[4]  * m[10] * m[15] +
              m[4]  * m[11] * m[14] +
              m[8]  * m[6]  * m[15] -
              m[8]  * m[7]  * m[14] -
              m[12] * m[6]  * m[11] +
              m[12] * m[7]  * m[10];

    inv[8] = m[4]  * m[9] * m[15] -
             m[4]  * m[11] * m[13] -
             m[8]  * m[5] * m[15] +
             m[8]  * m[7] * m[13] +
             m[12] * m[5] * m[11] -
             m[12] * m[7] * m[9];

    inv[12] = -m[4]  * m[9] * m[14] +
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] -
               m[8]  * m[6] * m[13] -
               m[12] * m[5] * m[10] +
               m[12] * m[6] * m[9];

    inv[1] = -m[1]  * m[10] * m[15] +
              m[1]  * m[11] * m[14] +
              m[9]  * m[2] * m[15] -
              m[9]  * m[3] * m[14] -
              m[13] * m[2] * m[11] +
              m[13] * m[3] * m[10];

    inv[5] = m[0]  * m[10] * m[15] -
             m[0]  * m[11] * m[14] -
             m[8]  * m[2] * m[15] +
             m[8]  * m[3] * m[14] +
             m[12] * m[2] * m[11] -
             m[12] * m[3] * m[10];

    inv[9] = -m[0]  * m[9] * m[15] +
              m[0]  * m[11] * m[13] +
              m[8]  * m[1] * m[15] -
              m[8]  * m[3] * m[13] -
              m[12] * m[1] * m[11] +
              m[12] * m[3] * m[9];

    inv[13] = m[0]  * m[9] * m[14] -
              m[0]  * m[10] * m[13] -
              m[8]  * m[1] * m[14] +
              m[8]  * m[2] * m[13] +
              m[12] * m[1] * m[10] -
              m[12] * m[2] * m[9];

    inv[2] = m[1]  * m[6] * m[15] -
             m[1]  * m[7] * m[14] -
             m[5]  * m[2] * m[15] +
             m[5]  * m[3] * m[14] +
             m[13] * m[2] * m[7] -
             m[13] * m[3] * m[6];

    inv[6] = -m[0]  * m[6] * m[15] +
              m[0]  * m[7] * m[14] +
              m[4]  * m[2] * m[15] -
              m[4]  * m[3] * m[14] -
              m[12] * m[2] * m[7] +
              m[12] * m[3] * m[6];

    inv[10] = m[0]  * m[5] * m[15] -
              m[0]  * m[7] * m[13] -
              m[4]  * m[1] * m[15] +
              m[4]  * m[3] * m[13] +
              m[12] * m[1] * m[7] -
              m[12] * m[3] * m[5];

    inv[14] = -m[0]  * m[5] * m[14] +
               m[0]  * m[6] * m[13] +
               m[4]  * m[1] * m[14] -
               m[4]  * m[2] * m[13] -
               m[12] * m[1] * m[6] +
               m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] +
              m[1] * m[7] * m[10] +
              m[5] * m[2] * m[11] -
              m[5] * m[3] * m[10] -
              m[9] * m[2] * m[7] +
              m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] -
             m[0] * m[7] * m[10] -
             m[4] * m[2] * m[11] +
             m[4] * m[3] * m[10] +
             m[8] * m[2] * m[7] -
             m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] +
               m[0] * m[7] * m[9] +
               m[4] * m[1] * m[11] -
               m[4] * m[3] * m[9] -
               m[8] * m[1] * m[7] +
               m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] -
              m[0] * m[6] * m[9] -
              m[4] * m[1] * m[10] +
              m[4] * m[2] * m[9] +
              m[8] * m[1] * m[6] -
              m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    if (det == 0)
        return false;

    det = 1.0 / det;

    for (i = 0; i < 16; i++)
        invOut[i] = inv[i] * det;

    return true;
}
