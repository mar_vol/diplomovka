#include "uniform.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define GLEW_STATIC
#include <GL/glew.h>

void iUniform::update(const int newValue)
{
    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        return;
    }
    if (newValue != value)
    {
        PROGRAM::setActualProgram(program);
        glUniform1i(loc, newValue);
        value = newValue;
    }
}

iUniform::iUniform(int program, const char* name, const int value)
{
    strncpy(this->name, name, 32);
    this->program = program;
    this->value = 0;
    PROGRAM::setActualProgram(program);
    loc = glGetUniformLocation(program, name);

    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        getchar();
    }
    else if (value != 0)
        update(value);
}


void fUniform::update(const float newValue)
{
    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        return;
    }
    if (newValue != value)
    {
        program::setActualProgram(program);
        glUniform1f(loc, newValue);
        value = newValue;
    }
}

fUniform::fUniform(int program, const char* name, const float value)
{
    strncpy(this->name, name, 32);
    this->program = program;
    this->value = 0.0f;
    PROGRAM::setActualProgram(program);
    loc = glGetUniformLocation(program, name);

    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        getchar();
    }
    else if (value != 0.0f)
        update(value);
}

void vec3Uniform::update(const float* newValue)
{
    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        return;
    }
    if (newValue == NULL)
    {
        fprintf(stderr, "value is NULL");
        return;
    }

    if (memcmp(newValue, value, sizeof(value)))
    {
        program::setActualProgram(program);
        glUniform3fv(loc, 1, newValue);
        memcpy(value, newValue, sizeof(value));
    }
}

vec3Uniform::vec3Uniform(int program, const char* name, const float* value)
{
    strncpy(this->name, name, 32);
    this->program = program;
    memset(this->value, 0, sizeof(this->value));
    PROGRAM::setActualProgram(program);
    loc = glGetUniformLocation(program, name);

    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        getchar();
    }
    else if (value != NULL)
        update(value);
}

void vec2Uniform::update(const float* newValue)
{
    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        return;
    }
    if (newValue == NULL)
    {
        fprintf(stderr, "value is NULL");
        return;
    }

    if (memcmp(newValue, value, sizeof(value)))
    {
        program::setActualProgram(program);
        glUniform2fv(loc, 1, newValue);
        memcpy(value, newValue, sizeof(value));
    }
}

vec2Uniform::vec2Uniform(int program, const char* name, const float* value)
{
    strncpy(this->name, name, 32);
    this->program = program;
    memset(this->value, 0, sizeof(this->value));
    PROGRAM::setActualProgram(program);
    loc = glGetUniformLocation(program, name);

    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        getchar();
    }
    else if (value != NULL)
        update(value);
}

void mat4Uniform::update(const float* newValue)
{
    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        return;
    }
    if (newValue == NULL)
    {
        fprintf(stderr, "value is NULL");
        return;
    }

    if (memcmp(newValue, value, sizeof(value)))
    {
        program::setActualProgram(program);
        glUniformMatrix4fv(loc, 1, GL_FALSE, newValue);
        memcpy(value, newValue, sizeof(value));
    }
}

mat4Uniform::mat4Uniform(int program, const char* name, const float* value)
{
    strncpy(this->name, name, 32);
    this->program = program;
    memset(this->value, 0, sizeof(this->value));
    PROGRAM::setActualProgram(program);
    loc = glGetUniformLocation(program, name);

    if (loc == -1)
    {
        fprintf(stderr, "Invalid name: %s for program: %d", name, program);
        getchar();
    }
    else if (value != NULL)
        update(value);
}

int program::actualProgram = 0;

program::program(int programID)
{
    this->programID = programID;
}

program::~program()
{
    if (programID > 0)
        glDeleteProgram(programID);
}
void program::use()
{
    if (programID < 0)
    {
        fprintf(stderr, "Invalid program: %d", programID);
        getchar();
    }
    else
    {
        setActualProgram(programID);
    }
}

int program::getActualProgram()
{
    return actualProgram;
}

void program::setActualProgram(int programID)
{
    int pr;
    glGetIntegerv(GL_CURRENT_PROGRAM, &pr);

    if (pr != actualProgram)
    {
        printf("pr: %d act: %d\n", pr, actualProgram);
        //getchar();
        glUseProgram(programID);
        actualProgram = programID;
    }

    if (programID != actualProgram)
    {
        glUseProgram(programID);
        actualProgram = programID;
    }
}
