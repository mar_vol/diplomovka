uniform float TessLevel;

int OsdPrimitiveIdBase()
{
    return 0;
}

int OsdGregoryQuadOffsetBase()
{
    return 0;
}

int OsdBaseVertex()
{
    return 0;
}

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelViewProjectionMatrix;

mat4 OsdModelViewMatrix()
{
    return modelViewMatrix;
}

mat4 OsdProjectionMatrix()
{
    return projectionMatrix;
}

mat4 OsdModelViewProjectionMatrix()
{
    return projectionMatrix*modelViewMatrix;
}

#ifdef VERTEX_SHADER

in vec3 position;
in vec2 uv;
out vec3 pos;


out UV_DATA
{
    vec2 uv;
}uv_out;

void main()
{
	gl_Position = vec4(position, 1.0);
    pos = position;
    uv_out.uv = uv;

}

#endif
//////////////////////////////////////////////
#ifdef TESS_CONTROL_SHADER

layout (vertices = 16) out;
in vec3 pos[];

in UV_DATA
{
    vec2 uv;
}uv_in[];

out UV_DATA_TES
{
    vec2 uv;
}uv_out[];

patch out vec4 tessOuterLo[1], tessOuterHi[1];
out OsdPerPatchVertexBezier v[];

uniform sampler2D tessTexture;
uniform int tessType;
uniform float tessFactor;
uniform float tessOuter;

float level;

float OsdTessLevel()
{
    return tessFactor*level;
}

void main()
{
	// Get a patch param from texture buffer.
	ivec3 patchParam = OsdGetPatchParam(gl_PrimitiveID);

	// Compute per-patch vertices.

	vec3 pos2[16];
	OsdPerPatchVertexBezier pos3[16];

	for (int i = 0; i < 16; i++)
		pos2[i] = pos[i];

	OsdComputePerPatchVertexBSpline(patchParam, gl_InvocationID, pos2, v[gl_InvocationID]);

	// Compute tessellation factors.
	if (gl_InvocationID == 0) {
        vec4 p[12];
        vec3 posE[12];
        float l[12];
        vec3 P = vec3(0), dPu = vec3(0), dPv = vec3(0);
        vec3 N = vec3(0), dNu = vec3(0), dNv = vec3(0);


		vec4 tessLevelOuter = vec4(0);
		vec2 tessLevelInner = vec2(0);

        vec2 uv = 0.25*(uv_in[5].uv + uv_in[6].uv + uv_in[9].uv + uv_in[10].uv);


        for (int i = 0; i < 16; i++)
            OsdComputePerPatchVertexBSpline(patchParam,  i, pos2, pos3[i]);

        OsdEvalPatchBezier(patchParam, vec2(0.5, 0.0), pos3, posE[0], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(1.0, 0.5), pos3, posE[1], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(0.5, 1.0), pos3, posE[2], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(0.0, 0.5), pos3, posE[3], dPu, dPv, N, dNu, dNv);
        ///////////////////////////////////////////////////
        OsdEvalPatchBezier(patchParam, vec2(0.25, 0.0 ), pos3, posE[ 4], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(0.75, 0.0 ), pos3, posE[ 5], dPu, dPv, N, dNu, dNv);

        OsdEvalPatchBezier(patchParam, vec2(1.0 , 0.25), pos3, posE[ 6], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(1.0 , 0.75), pos3, posE[ 7], dPu, dPv, N, dNu, dNv);

        OsdEvalPatchBezier(patchParam, vec2(0.25, 1.0 ), pos3, posE[ 8], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(0.75, 1.0 ), pos3, posE[ 9], dPu, dPv, N, dNu, dNv);

        OsdEvalPatchBezier(patchParam, vec2(0.0 , 0.25), pos3, posE[10], dPu, dPv, N, dNu, dNv);
        OsdEvalPatchBezier(patchParam, vec2(0.0 , 0.75), pos3, posE[11], dPu, dPv, N, dNu, dNv);


        for (int i = 0; i < 12; i++)
        {
            p[i] = modelViewProjectionMatrix*vec4(posE[i], 1.0);
            l[i] = 5/p[i].w;
        }

		level = texture(tessTexture, uv).r * 0.25*(l[0] + l[1] + l[2] + l[3]);

		OsdGetTessLevelsUniform(patchParam,
								tessLevelOuter, tessLevelInner,
								tessOuterLo[0], tessOuterHi[0]);

        int transitionMask;
        int boundaryMask;

        switch (tessType)
        {
            case 0:
                gl_TessLevelOuter[0] = tessLevelOuter[0];
                gl_TessLevelOuter[1] = tessLevelOuter[1];
                gl_TessLevelOuter[2] = tessLevelOuter[2];
                gl_TessLevelOuter[3] = tessLevelOuter[3];
                break;
            case 1:
                gl_TessLevelOuter[0] = tessOuter;
                gl_TessLevelOuter[1] = tessOuter;
                gl_TessLevelOuter[2] = tessOuter;
                gl_TessLevelOuter[3] = tessOuter;
                break;
            case 2:
                transitionMask = OsdGetPatchTransitionMask(patchParam);
                gl_TessLevelOuter[0] = tessOuter * (((transitionMask&8)>>3) + 1);
                gl_TessLevelOuter[1] = tessOuter * (( transitionMask&1)     + 1);
                gl_TessLevelOuter[2] = tessOuter * (((transitionMask&2)>>1) + 1);
                gl_TessLevelOuter[3] = tessOuter * (((transitionMask&4)>>2) + 1);
                break;
            case 3:
                transitionMask = OsdGetPatchTransitionMask(patchParam);

                boundaryMask = OsdGetPatchBoundaryMask(patchParam);

                int refinementLevel = OsdGetPatchRefinementLevel(patchParam);
                float c = 1.0 / pow(2, refinementLevel-1);
                const float c2 = 0.5*c;



                //vlavo
                if (((boundaryMask&8)>>3) == 1)
                {
                    tessOuterLo[0][0] = round(0.5*tessLevelInner[1] + 0.5);
                    tessOuterHi[0][0] = tessOuterLo[0][0];
                    gl_TessLevelOuter[0] = 2*tessOuterLo[0][0];
                }
                else
                {
                    if (((transitionMask&8)>>3) == 1)
                    {
                        tessOuterLo[0][0] = max(round(c2*tessFactor*texture(tessTexture, 0.75*uv_in[5].uv + 0.25*uv_in[9].uv).r*l[10]), 1.0);
                        tessOuterHi[0][0] = max(round(c2*tessFactor*texture(tessTexture, 0.25*uv_in[5].uv + 0.75*uv_in[9].uv).r*l[11]), 1.0);
                        gl_TessLevelOuter[0] = tessOuterLo[0][0] + tessOuterHi[0][0];
                    }
                    else
                        gl_TessLevelOuter[0] = max(round(c*tessFactor*texture(tessTexture, 0.5*(uv_in[5].uv + uv_in[9].uv)).r*l[3]), 1.0);
                }

                //dole
                if ((boundaryMask&1) == 1)
                {
                    tessOuterLo[0][1] = round(0.5*tessLevelInner[0] + 0.5);
                    tessOuterHi[0][1] = tessOuterLo[0][1];
                    gl_TessLevelOuter[1] = 2*tessOuterLo[0][1];
                }
                else
                {
                    if ((transitionMask&1) == 1)
                    {
                        tessOuterLo[0][1] = max(round(c2*tessFactor*texture(tessTexture, 0.75*uv_in[5].uv + 0.25*uv_in[6].uv).r*l[4]), 1.0);
                        tessOuterHi[0][1] = max(round(c2*tessFactor*texture(tessTexture, 0.25*uv_in[5].uv + 0.75*uv_in[6].uv).r*l[5]), 1.0);
                        gl_TessLevelOuter[1] = tessOuterLo[0][1] + tessOuterHi[0][1];
                    }
                    else
                        gl_TessLevelOuter[1] = max(round(c*tessFactor*texture(tessTexture, 0.5*(uv_in[5].uv + uv_in[6].uv)).r*l[0]), 1.0);
                }

                //vpravo
                if (((boundaryMask&2)>>1) == 1)
                {
                    tessOuterLo[0][2] = round(0.5*tessLevelInner[1] + 0.5);
                    tessOuterHi[0][2] = tessOuterLo[0][2];
                    gl_TessLevelOuter[2] = 2*tessOuterLo[0][2];
                }
                else
                {
                    if (((transitionMask&2)>>1) == 1)
                    {
                        tessOuterLo[0][2] = max(round(c2*tessFactor*texture(tessTexture, 0.75*uv_in[6].uv + 0.25*uv_in[10].uv).r*l[6]), 1.0);
                        tessOuterHi[0][2] = max(round(c2*tessFactor*texture(tessTexture, 0.25*uv_in[6].uv + 0.75*uv_in[10].uv).r*l[7]), 1.0);
                        gl_TessLevelOuter[2] = tessOuterLo[0][2] + tessOuterHi[0][2];
                    }
                    else
                        gl_TessLevelOuter[2] = max(round(c*tessFactor*texture(tessTexture, 0.5*(uv_in[6].uv + uv_in[10].uv)).r*l[1]), 1.0);
                }
                //hore
                if (((boundaryMask&4)>>2) == 1)
                {
                    tessOuterLo[0][3] = round(0.5*tessLevelInner[0] + 0.5);
                    tessOuterHi[0][3] = tessOuterLo[0][3];
                    gl_TessLevelOuter[3] = 2*tessOuterLo[0][3];
                }
                else
                {
                    if (((transitionMask&4)>>2) == 1)
                    {
                        tessOuterLo[0][3] = max(round(c2*tessFactor*texture(tessTexture, 0.75*uv_in[9].uv + 0.25*uv_in[10].uv).r*l[8]), 1.0);
                        tessOuterHi[0][3] = max(round(c2*tessFactor*texture(tessTexture, 0.25*uv_in[9].uv + 0.75*uv_in[10].uv).r*l[9]), 1.0);
                        gl_TessLevelOuter[3] = tessOuterLo[0][3] + tessOuterHi[0][3];
                    }
                    else
                        gl_TessLevelOuter[3] = max(round(c*tessFactor*texture(tessTexture, 0.5*(uv_in[9].uv + uv_in[10].uv)).r*l[2]), 1.0);
                }
                break;
            case 4:
                gl_TessLevelOuter[0] = 1;
                gl_TessLevelOuter[1] = 1;
                gl_TessLevelOuter[2] = 1;
                gl_TessLevelOuter[3] = 1;

                gl_TessLevelInner[0] = 1;
                gl_TessLevelInner[1] = 1;
                uv_out[gl_InvocationID].uv = uv_in[gl_InvocationID].uv;
                return;
        }

		gl_TessLevelInner[0] = tessLevelInner[0];
		gl_TessLevelInner[1] = tessLevelInner[1];
	}
    uv_out[gl_InvocationID].uv = uv_in[gl_InvocationID].uv;
}
#endif
//////////////////////////////////////////
#ifdef TESS_EVALUATION_SHADER

layout(quads) in;
patch in vec4 tessOuterLo[1], tessOuterHi[1];
in OsdPerPatchVertexBezier v[];

uniform float midStrength;
uniform float strength;

uniform sampler2D displaceTexture;
uniform sampler2D displaceTexture2;
uniform sampler2D normalTexture;
uniform float t;

in UV_DATA_TES
{
    vec2 uv;
}uv_in[];

out UV_DATA_FRAG
{
    vec2 uv;
    vec3 dPu, dPv, N;
}uv_out;

out vec3 norm;

out vec3 deb;
out vec3 feedback_position;
out vec3 feedback_normal;
out vec2 feedback_uv;

out vec3 Position;

void interpolateUV(in vec2 UV, out vec2 uv)
//vstup UV - koordinat patchu
//vystup uv - koordinat textury
{
    vec2 uv1, uv2;

    uv1 = mix(uv_in[ 5].uv, uv_in[ 6].uv, UV.x);
    uv2 = mix(uv_in[ 9].uv, uv_in[10].uv, UV.x);

    uv = mix(uv1, uv2, UV.y);
}

vec4 getTextureValue(vec2 uv)
{
    vec4 val1 = texture(displaceTexture, uv);
    vec4 val2 = texture(displaceTexture2, uv);

    return mix(val1, val2, t);
}

void main()
{
	// Compute tesscoord.
	vec2 UV = OsdGetTessParameterization(gl_TessCoord.xy, tessOuterLo[0], tessOuterHi[0]);

	vec3 P = vec3(0), dPu = vec3(0), dPv = vec3(0);
	vec3 N = vec3(0), dNu = vec3(0), dNv = vec3(0);
	ivec3 patchParam = v[0].patchParam;

	OsdPerPatchVertexBezier v2[16];

	for (int i = 0; i < 16; i++)
		v2[i] = v[i];

	// Evaluate patch at the tess coord UV
	OsdEvalPatchBezier(patchParam, UV, v2, P, dPu, dPv, N, dNu, dNv);

	dPu = normalize(dPu);
    dPv = normalize(dPv);
    N    = normalize(N);

	// Apply model-view-projection matrix.

	//float c = texelFetch(OsdFVarDataBuffer, 0).r;

    interpolateUV(UV, uv_out.uv);
    vec3 dPos = strength*(getTextureValue(uv_out.uv).xyz - midStrength);

    //dPos.x = 0;
    //dPos.y = 0;

    mat3 patchMatrix = mat3(dPu, dPv, N);
    vec3 dPos2 = patchMatrix*dPos;

	gl_Position = modelViewProjectionMatrix*vec4(P + dPos2, 1);
	Position = (modelViewMatrix*vec4(P + dPos2, 1)).xyz;

    feedback_position = P + dPos2;
    feedback_normal = N;
    interpolateUV(UV, feedback_uv);

	uv_out.N = N;
	uv_out.dPu = dPu;
	uv_out.dPv = dPv;

    //uv_out.uv = UV;
}
#endif
/////////////////////////////////////
#ifdef GEOMETRY_SHADER

void main()
{
}
#endif
///////////////////////////////////////////
#ifdef FRAGMENT_SHADER

in vec3 pos;
out vec4 color;

uniform sampler2D normalTexture;

in UV_DATA_FRAG
{
    vec2 uv;
    vec3 dPu, dPv, N;
}uv_in;

in vec3 norm;
in vec3 deb;
in vec3 Position;

uniform sampler2D diffTexture;

struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	//vec3 emission;
	float shininess;
	float reflection;
};

struct SunLight
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	vec3 direction;

};

uniform Material material;
uniform SunLight sun;


void calculateSunLight(inout vec3 ambient, inout vec3 diffuse, inout vec3 specular, in vec3 N, in vec3 V)
{
	ambient  += sun.ambient;

    diffuse  += max(dot(-sun.direction, N), 0)*sun.diffuse;
    vec3 S = normalize(-sun.direction - V);
    specular += sun.specular * pow (max(dot(S, N) , 0) , material.shininess);

}



void main()
{
    mat3 patchMatrix = mat3(normalize(uv_in.dPu), normalize(uv_in.dPv), normalize(uv_in.N));

    //color = vec4(0.5*uv_in.uv + 0.5, 0.0, 1.0);
    //return;

    vec3 normalLocal = 2*texture2D(normalTexture, uv_in.uv).xyz - 1;
    vec3 normalGlobal = mat3(modelViewMatrix)*patchMatrix*normalLocal;//patchMatrix*normalLocal;

    vec4 ambient  = vec4(0.0, 0.0, 0.0, 1.0);
	vec4 diffuse  = vec4(0.0, 0.0, 0.0, 1.0);
	vec4 specular = vec4(0.0, 0.0, 0.0, 1.0);




    vec3 V = normalize(Position);

    if (dot(-sun.direction, normalGlobal) < 0)
        normalGlobal = -normalGlobal;

    vec3 norm2 = normalize(normalGlobal);
    calculateSunLight(ambient.rgb, diffuse.rgb, specular.rgb, norm2, V);

	//color = texture(diffTexture, uv_in.uv)*(max(0.5*-norm2.x,0)+0.5);

    //color = vec4(0.5*deb + 0.5, 1.0);
    //color = vec4(0.8, 0.8, 0.8, 1.0)*norm2.z;//(max(0.5*-norm2.x,0)+0.5);

	//color = vec4(norm, 1);
	//color = vec4(uv_in.uv.x, uv_in.uv.y, 0.0, 1.0);

    ambient   *= vec4(material.ambient,  1.0);
    diffuse   *= vec4(material.diffuse,  1.0);
    specular  *= vec4(material.specular, 1.0);

	color = ambient + diffuse + specular;
	color.a = 1.0;
}
#endif
