
#ifdef VERTEX_SHADER

in layout(location = 0) vec2 uv;
in layout(location = 1) vec2 uv2;
in layout(location = 2) vec2 off;

out vec2 UV;
out vec2 UV2;
out vec2 OFF;


void main(void)
{
    UV = uv;
    UV2 = uv2;
    OFF = off;
}
#endif

#ifdef GEOMETRY_SHADER

in vec2 UV[];
in vec2 UV2[];
in vec2 OFF[];

layout (points) in;
layout (triangle_strip) out;
layout (max_vertices = 4) out;

uniform vec2 fStart;
uniform float fBase;
uniform float fHeight;
uniform float fAspect;

uniform sampler1D pos;

out vec2 uv;

void main()
{
    int i;
    float hb = fHeight/fBase;
    float h = (UV2[0].t - UV[0].t)*hb;
    float w = (UV2[0].s - UV[0].s)*hb/fAspect;

    float x = fStart.x + hb*(texelFetch(pos, gl_PrimitiveIDIn, 0).r + OFF[0].x)/fAspect;
    float y = fStart.y + hb*OFF[0].y;

    for (i = 0; i < 4; i++)
    {
        switch(i)
        {
            case 0:
                uv =  UV[0];
                gl_Position = vec4(x, y, 0.0, 1.0);
                break;
            case 1:
                uv.s = UV2[0].s;
                uv.t = UV[0].t;
                gl_Position = vec4(x + w,  y, 0.0, 1.0);
                break;
            case 2:
                uv.s = UV[0].s;
                uv.t = UV2[0].t;
                gl_Position = vec4(x,  y + h, 0.0, 1.0);

                break;
            default:
                uv = UV2[0];
                gl_Position = vec4(x + w,  y + h,  0.0, 1.0);
                break;
        }

        EmitVertex();
    }
    EndPrimitive();
}

#endif // GEOMETRY_SHADER

#ifdef FRAGMENT_SHADER

in vec2 uv;
uniform sampler2D fontText;

out vec4 color;

void main()
{
    float value = 1 - texture(fontText, uv).r;
    const float width = 0.5;
    const float edge = 0.05;
    float alpha = 1.0 - smoothstep(width, width + edge, value);
    color = vec4(1.0, 1.0, 1.0, alpha);
}
#endif

