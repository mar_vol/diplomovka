#ifdef VERTEX_SHADER

void main()
{
}

#endif
//////////////////////////////////////////////
#ifdef TESS_CONTROL_SHADER

layout (vertices = 16) out;
in vec3 position[];
patch out vec4 tessOuterLo, tessOuterHi;
out OsdPerPatchVertexBezier v;

void main()
{
	// Get a patch param from texture buffer.
	ivec3 patchParam = OsdGetPatchParam(gl_PrimitiveID);

	// Compute per-patch vertices.
	OsdComputePerPatchVertexBSpline(patchParam, gl_InvocationID, position, v);

	// Compute tessellation factors.
	if (gl_InvocationID == 0) {
		vec4 tessLevelOuter = vec4(0);
		vec2 tessLevelInner = vec2(0);
		OsdGetTessLevelsUniform(patchParam,
								tessLevelOuter, tessLevelInner,
								tessOuterLo, tessOuterHi);

		gl_TessLevelOuter[0] = tessLevelOuter[0];
		gl_TessLevelOuter[1] = tessLevelOuter[1];
		gl_TessLevelOuter[2] = tessLevelOuter[2];
		gl_TessLevelOuter[3] = tessLevelOuter[3];

		gl_TessLevelInner[0] = tessLevelInner[0];
		gl_TessLevelInner[1] = tessLevelInner[1];
	}
}
#endif
//////////////////////////////////////////
#ifdef TESS_EVALUATION_SHADER

layout(quads) in;
patch in vec4 tessOuterLo, tessOuterHi;
in OsdPerPatchVertexBezier v[];
uniform mat4 mvpMatrix;

void main()
{
	// Compute tesscoord.
	vec2 UV = OsdGetTessParameterization(gl_TessCoord.xy, tessOuterLo, tessOuterHi);

	vec3 P = vec3(0), dPu = vec3(0), dPv = vec3(0);
	vec3 N = vec3(0), dNu = vec3(0), dNv = vec3(0);
	ivec3 patchParam = v[0].patchParam;

	// Evaluate patch at the tess coord UV
	OsdEvalPatchBezier(patchParam, UV, v, P, dPu, dPv, N, dNu, dNv);

	// Apply model-view-projection matrix.
	gl_Position = mvpMatrix * vec4(P, 1);
}
#endif
/////////////////////////////////////
#ifdef GEOMETRY_SHADER

void main()
{
}
#endif
///////////////////////////////////////////
#ifdef FRAGMENT_SHADER

out vec4 color;

void main()
{
	color = vec4(1.0, 0.0, 0.0, 1.0);
}
#endif

