#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>

#include <opensubdiv/osd/mesh.h>
#include <opensubdiv/osd/glMesh.h>

#include <opensubdiv/far/topologyDescriptor.h>

#include "../shader/uniform.h"

struct UV_Vertex
{
    void Clear();
    void AddWithWeight(UV_Vertex const & src, float weight);
    float u,v;
};

class Object
{
    public:
        Object(const char* file, PROGRAM* program, PROGRAM* normalProgram, PROGRAM* controlHullProgram, int subdivisionLevel = 4, bool CPU = true);
        ~Object();
        void render(bool normalMap = false, const char* outputFile = NULL);
        void renderControlHull(I_UNIFORM* line);
        int getNearestVertex(float xNDC, float yNDC, const float* MVP, float radius);

        void startMovingVertex(int id, float xNDC, float yNDC, const float* MVP);
        void cancelMovingVertex();
        void moveVertex(float xNDC, float yNDC);
        void update();
    private:
        int loadObjFromFile(const char* file);

        void initOpenSubdiv();
        void initOpenGL();

        void printIndexBuffer();
        void printUvBuffer();
        void printPosBuffer();
        void printFaceBuffer();
        void deallocate();
        void saveToFile(const char* file);

        std::vector <float>pos;
        std::vector <float>uv;
        std::vector<unsigned int>index;
        std::vector<int> numVerticesPerFace;
        std::vector<UV_Vertex> fvBufferUV;

        std::vector <int> lines;

        PROGRAM* program;
        PROGRAM* normalProgram;
        PROGRAM* controlHullProgram;

        int numVertices;
        int numUV;
        int numFaces;
        int subdivisionLevel;
        bool initialized, CPU;

        GLuint VAO, VAOupdate;
        GLuint VBO;
        GLuint EBO;

        GLuint VBOcontrol;
        GLuint VAOcontrol;
        GLuint EBOcontrol;
        GLuint uvBuffer;

        static const int numVertexElements = 3;

        OpenSubdiv::Osd::GLMeshInterface* mesh;
        OpenSubdiv::Far::TopologyDescriptor topologyDescriptor;
        OpenSubdiv::Far::TopologyRefiner* topologyRefiner;

        struct MovingState
        {
            float startPos[4];
            float iMatrix[16];
            float xNDC, yNDC;
            int id;
            bool moving;
        }mState;
};

#endif // _OBJECT_H_
