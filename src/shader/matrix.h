#ifndef _MATRIX_H_
#define _MATRIX_H_

#ifndef NULL
    #define NULL 0
#endif

void printMatrix(const float *m, const int n);
inline void multipleMatrix(float *m_out, const float* m1, const float* m2);
inline void copyMatrix(float *m_out, const float *m_in, const int n);

void translate(const float g_x, const float g_y, const float g_z, float *m = NULL);
void scale(const float s_x, const float s_y, const float s_z, float *m = NULL);

void rotateX(const float a, float *m = NULL);
void rotateY(const float a, float *m = NULL);
void rotateZ(const float a, float *m = NULL);

void loadIdentity(float *m = NULL);
void ortho(float left, float right, float top, float bottom);
void ortho(float left, float right, float top, float bottom, float nearVal, float farVal);
void perspective(float fovy,  float aspect, float zNear, float zFar);

void getProjectionMatrix(float *m);
void getModelViewMatrix(float *m);

const float *getModelViewMatrix();
const float *getProjectionMatrix();
const float *getModelViewProjectionMatrix();

void getPoint  (float *p_out, const float *p_in, const float *m);
void getPointW (float *p_out, const float *p_in, const float *m);
void getPointW4(float *p_out, const float *p_in, const float *m);

bool invertMatrix(const float m[16], float invOut[16]);

#endif // _MATRIX_H_
