#define GLEW_STATIC
#include <GL/glew.h>
#define GLFW_DLL
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>

#include "../shader/shader.h"
#include "../shader/uniform.h"
#include "../obj/object.h"
#include "font.h"
#include "gui.h"


#include <opensubdiv/osd/cpuPatchTable.h>

#include "../shader/matrix.h"

#define FBO_WIDTH 128
#define NORMAL_MAP_WIDTH 1024

static void callbackError(OpenSubdiv::Far::ErrorType err, const char *message)
{
    fprintf(stderr, "Error: %d (%s)\n", err, message);
    getchar();
}

void error_callback(int error, const char* description)
{
    fprintf(stderr, description);
    getchar();
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    static int id = -1;
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    ypos = gui.h - ypos - 1;

    xpos = 2*xpos/gui.w - 1;
    ypos = 2*ypos/gui.h - 1;
    if (button == GLFW_MOUSE_BUTTON_MIDDLE || (button == GLFW_MOUSE_BUTTON_LEFT && !gui.editing))
    {
        if (action == GLFW_PRESS)
        {
            gui.rotButtonPress     = true;
            gui.rotButtonPressInit = true;
        }

        if (action == GLFW_RELEASE)
        {
            gui.rotButtonPress = false;
        }

    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && gui.editing)
    {
        if (action == GLFW_PRESS && id >= 0)
        {
            gui.moving = true;
            printf("id: %d\n", id);
            gui.obj->startMovingVertex(id, xpos, ypos, getModelViewProjectionMatrix());
        }
        if (action == GLFW_RELEASE)
        {
            gui.moving = false;
            gui.obj->cancelMovingVertex();
        }
    }


    if (button == GLFW_MOUSE_BUTTON_RIGHT && gui.editing)
    {


        int vert = gui.obj->getNearestVertex(xpos, ypos, getModelViewProjectionMatrix(), 0.05);
        programUniforms.selectedVertex->update(vert);

        printf("%.3f %.3f vert: %d\n", xpos, ypos, vert);
        id = vert;

        //int selectedVertex = gui.obj->getNearestVertex()
    }
}

void scroll_callback(GLFWwindow* window, double x, double y)
{
    if (y > 0)
        gui.distance /= 1.1;
    else
        gui.distance *= 1.1;
}

void mouse_motion_callback(GLFWwindow* window, double x, double y)
{
    double xpos, ypos;
    if (gui.rotButtonPress)
    {
        if (gui.rotButtonPressInit)
        {
            gui._x = x;
            gui._y = y;
            gui.rotButtonPressInit = false;
        }
        else
        {
            gui.rX += (float) (y - gui._y);
            gui.rY += (float) (x - gui._x);

            gui._x = x;
            gui._y = y;

            //printf("Rx: %.3f Ry: %3f\n", gui.rX, gui.rY);
        }
    }

    if (gui.moving)
    {
        if (gui.editing == false)
        {
            gui.obj->cancelMovingVertex();
            gui.moving = false;
            return;
        }

        glfwGetCursorPos(window, &xpos, &ypos);
        ypos = gui.h - ypos - 1;

        xpos = 2*xpos/gui.w - 1;
        ypos = 2*ypos/gui.h - 1;
        gui.obj->moveVertex(xpos, ypos);

        printf("AAAAA\n");
    }
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    float t;
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case '\t':
        case 258:
            gui.editing = !gui.editing;
            break;
        case 'W':
        case 'w':
            gui.wireframe = !gui.wireframe;
            if   (gui.wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            else                 glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            break;
        case 'T':
        case 't':
            gui.textureView = (gui.textureView+1) % 4;
            break;
        case 'N':
        case 'n':
            programUniforms.dispStrength->update(programUniforms.dispStrength->value - 0.05);
            programUniforms.dispStrengthNM->update(programUniforms.dispStrength->value);
            break;
        case 'M':
        case 'm':
            programUniforms.dispStrength->update(programUniforms.dispStrength->value + 0.05);
            programUniforms.dispStrengthNM->update(programUniforms.dispStrength->value);
            break;
        case 'D':
        case 'd':
            if (gui.displaceTextureCount > 0)
            {
                gui.currentDisplaceTexture = (gui.currentDisplaceTexture + 1)%gui.displaceTextureCount;
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, gui.displaceTextures[gui.currentDisplaceTexture]);
            }
            break;
        case 'E':
        case 'e':
            programUniforms.tessType->update((programUniforms.tessType->value + 1)%5);
            printf("Tess type: %d\n", programUniforms.tessType->value);
            break;
        case 'K':
        case 'k':
            programUniforms.tessFactor->update(programUniforms.tessFactor->value + 1);
            break;
        case 'J':
        case 'j':
            programUniforms.tessFactor->update(programUniforms.tessFactor->value - 1);
            break;
        case 'O':
        case 'o':
            programUniforms.tessOuter->update(programUniforms.tessOuter->value - 1);
            break;
        case 'P':
        case 'p':
            programUniforms.tessOuter->update(programUniforms.tessOuter->value + 1);
            break;
        case 'C':
        case 'c':
            t = programUniforms.t->value - 0.05;
            t = (t < 0.0)?0.0:t;
            programUniforms.t  ->update(t);
            programUniforms.tNM->update(t);
            break;
        case 'V':
        case 'v':
            t = programUniforms.t->value + 0.05;
            t = (t > 1.0)?1.0:t;
            programUniforms.t  ->update(t);
            programUniforms.tNM->update(t);
            break;
        case 'X':
        case 'x':
            gui.textI = (gui.textI + 1)%4;
            glActiveTexture(GL_TEXTURE3);

            if (gui.textI != 3)
                glBindTexture(GL_TEXTURE_2D, gui.text[gui.textI]);
            else
                glBindTexture(GL_TEXTURE_2D, gui.displaceTextures[gui.currentDisplaceTexture]);;

            printf("Tex: %d", gui.textI);
            break;
        case 'G':
        case 'g':
            gui.obj->render(false, "debugOutput.obj");
            break;
        }
    }
}

void initGLEW()
{
    glewExperimental = true;
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        printf("Error: %s\n", glewGetErrorString(err));
        getchar();
        exit(-1);
    }
    printf("Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
    if (GLEW_ARB_vertex_program)
    {
        printf("Status: GLEW_ARB_vertex_program\n");
    }
    else
    {
        printf("Error: GLEW_ARB_vertex_program\n");
        getchar();
        return;
        //exit(-1);
    }
    if (GLEW_VERSION_1_3)
    {
        printf("Status: Yay! OpenGL 1.3 is supported!\n");
    }
    else
    {
        printf("Status: Sorry! OpenGL 1.3 is not supported!\n");
        getchar();
        //exit(-1);
    }
    if (glewIsSupported("GL_VERSION_1_4  GL_ARB_point_sprite"))
    {
        printf("Great, we have OpenGL 1.4 + point sprites.\n");
    }
    else
        exit(-1);
    if (glewGetExtension("GL_ARB_fragment_program"))
    {
        printf("Looks like ARB_fragment_program is supported..\n");
    }
    else
        exit(-1);
}

int texture;
GLuint uvBuffer;
int tessTexture;
int disp2Texture;
int normalTexture;
GLuint normalFBO;

#define CPU_REFINER

GLuint laplaceVBO;
GLuint laplaceEBO;
GLuint laplaceFBO;

void initGL()
{

    GLuint BSplinePatchProgram;
    GLuint LaplaceProgram;
    GLuint TextureViewProgram;
    GLuint NormalMapProgram;
    GLuint controlHullProgram;
    GLuint fontProgram;

    initGLEW();
    glClearColor(0.5, 0.5, 0.5, 1.0);
    //glClearColor(1.0, 1.0, 1.0, 1.0);
    //glClearColor(0.0, 0.0, 0.0, 1.0);

    if (glCreateProgram == NULL)
        fprintf(stderr, "NULL\n");

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));

    BSplinePatchProgram = loadShaders("shaders/shaderTess.glsl", true, false, true);
    LaplaceProgram = loadShaders("shaders/laplace.glsl");
    TextureViewProgram = loadShaders("shaders/textureView.glsl");
    NormalMapProgram = loadShaders("shaders/normalMapGen.glsl", true);
    controlHullProgram = loadShaders("shaders/controlHull.glsl");
    fontProgram = loadShaders("shaders/font.glsl", false, true);

    programs.normalMapGen = new PROGRAM(NormalMapProgram);

    programs.bspline = new PROGRAM(BSplinePatchProgram);
    programs.bspline->use();

    programUniforms.dispStrength    = new F_UNIFORM(BSplinePatchProgram, "strength");
    programUniforms.dispMidStrength = new F_UNIFORM(BSplinePatchProgram, "midStrength", 0.5);


    glActiveTexture(GL_TEXTURE1);
    tessTexture = generateTexture(NULL, false, FBO_WIDTH, FBO_WIDTH);
    I_UNIFORM tessTextureU(BSplinePatchProgram, "tessTexture", 1);

    glActiveTexture(GL_TEXTURE7);
    disp2Texture = generateTexture("res/textures/canyon.bmp", true);

    glActiveTexture(GL_TEXTURE0);

    programUniforms.modelView           = new MAT_4_UNIFORM(BSplinePatchProgram, "modelViewMatrix");
    //programUniforms.projection          = new MAT_4_UNIFORM(BSplinePatchProgram, "projectionMatrix");
    programUniforms.modelViewProjection = new MAT_4_UNIFORM(BSplinePatchProgram, "modelViewProjectionMatrix");

    float temp[3];

    temp[0] = 0.1; temp[1] = 0.18725; temp[2] = 0.1745;
    programUniforms.materialAmbient  = new VEC3_UNIFORM(BSplinePatchProgram, "material.ambient", temp);
    temp[0] = 0.396; temp[1] = 0.74151; temp[2] = 0.69102;
    programUniforms.materialDiffuse  = new VEC3_UNIFORM(BSplinePatchProgram, "material.diffuse", temp);
    temp[0] = 0.297254; temp[1] = 0.30829; temp[2] = 0.306678;
    programUniforms.materialSpecular = new VEC3_UNIFORM(BSplinePatchProgram, "material.specular", temp);
    programUniforms.materialShininess = new F_UNIFORM(BSplinePatchProgram, "material.shininess", 12.8);
    programUniforms.materialReflection = new F_UNIFORM(BSplinePatchProgram, "material.reflection", 0.1);


    temp[0] = 0.14; temp[1] = 0.14; temp[2] = 0.1;
    programUniforms.sunAmbient  = new VEC3_UNIFORM(BSplinePatchProgram, "sun.ambient", temp);
    temp[0] = 1.0; temp[1] = 0.95; temp[2] = 0.75;
    programUniforms.sunDiffuse  = new VEC3_UNIFORM(BSplinePatchProgram, "sun.diffuse", temp);
    temp[0] = 0.81; temp[1] = 0.69; temp[2] = 0.46;
    programUniforms.sunSpecular  = new VEC3_UNIFORM(BSplinePatchProgram, "sun.specular", temp);
    temp[0] = 0.0; temp[1] = 0.0; temp[2] = -1.0;
    programUniforms.sunDirection  = new VEC3_UNIFORM(BSplinePatchProgram, "sun.direction", temp);

    glEnable(GL_DEPTH_TEST);

    programs.laplace = new PROGRAM(LaplaceProgram);
    programs.laplace->use();

    float vertexData[] =
        //pos and uv
    {
        -1.0, -1.0, 0.0,    0.0, 0.0,
        1.0, -1.0, 0.0,    1.0, 0.0,
        1.0,  1.0, 0.0,    1.0, 1.0,
        -1.0,  1.0, 0.0,    0.0, 1.0
    };

    GLuint indices[] =
    {
        0, 1, 2,
        0, 2, 3
    };

    glGenBuffers(1, &laplaceVBO);
    glGenBuffers(1, &laplaceEBO);

    glBindBuffer(GL_ARRAY_BUFFER, laplaceVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData , GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, laplaceEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices , GL_STATIC_DRAW);

    printf("Laplace: %d\n", LaplaceProgram);
    F_UNIFORM duv(LaplaceProgram, "duv", 1.0/(FBO_WIDTH));

    glGenFramebuffers(1, &laplaceFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, laplaceFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tessTexture, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    I_UNIFORM inputTexture(LaplaceProgram, "inputTexture", 4);
    glActiveTexture(GL_TEXTURE3);

    gui.displaceTextures[gui.displaceTextureCount++] = generateTexture("res/textures/displace1.bmp", true);
    gui.displaceTextures[gui.displaceTextureCount++] = generateTexture("res/textures/displace2.bmp", true);
    gui.displaceTextures[gui.displaceTextureCount++] = generateTexture("res/textures/displace3.bmp", true);

    gui.text[0] = generateTexture("res/textures/displace_128.bmp", true);
    gui.text[1] = generateTexture("res/textures/displace_256.bmp", true);
    gui.text[2] = generateTexture("res/textures/displace_512.bmp", true);

    glBindTexture(GL_TEXTURE_2D, gui.displaceTextures[0]);

    glActiveTexture(GL_TEXTURE0);


    programs.texView = new PROGRAM(TextureViewProgram);
    programs.texView->use();

    programUniforms.textureView = new I_UNIFORM(TextureViewProgram, "inputTexture", 1);

    programs.bspline->use();

    I_UNIFORM dispText(BSplinePatchProgram, "displaceTexture",  3);
    I_UNIFORM dispText2(BSplinePatchProgram, "displaceTexture2", 7);

    programUniforms.tessType = new I_UNIFORM(BSplinePatchProgram, "tessType", 3);
    programUniforms.tessFactor = new F_UNIFORM(BSplinePatchProgram, "tessFactor", 20);
    programUniforms.tessOuter = new F_UNIFORM(BSplinePatchProgram, "tessOuter", 5);
    programUniforms.t = new F_UNIFORM(BSplinePatchProgram, "t", 0.0);


    PROGRAM::setActualProgram(BSplinePatchProgram);

    texture = generateTexture("res/textures/lena.bmp");
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texture);

    I_UNIFORM diffTex(BSplinePatchProgram, "diffTexture", 2);
    I_UNIFORM normalTex(BSplinePatchProgram, "normalTexture", 4);

    glActiveTexture(GL_TEXTURE4);
    normalTexture = generateTexture(NULL, true, NORMAL_MAP_WIDTH, NORMAL_MAP_WIDTH);

    glGenFramebuffers(1, &normalFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, normalFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, normalTexture, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    programUniforms.tessFactorNM      = new F_UNIFORM(NormalMapProgram, "tessFactor", 50);
    programUniforms.dispMidStrengthNM = new F_UNIFORM(NormalMapProgram, "midStrength", 0.5);
    programUniforms.dispStrengthNM    = new F_UNIFORM(NormalMapProgram, "strength");

    I_UNIFORM dispTextNM(NormalMapProgram, "displaceTexture", 3);
    I_UNIFORM dispText2NM(NormalMapProgram, "displaceTexture2", 7);
    programUniforms.tNM = new F_UNIFORM(NormalMapProgram, "t", 0.0);

    programs.simple = new PROGRAM(loadShaders("shaders/simple.glsl", false, false));

    programUniforms.testMVP = new MAT_4_UNIFORM(programs.simple->programID, "MVP");
    programUniforms.simpleText = new I_UNIFORM(programs.simple->programID, "tex", 4);

    programs.control = new PROGRAM(controlHullProgram);
    programUniforms.controlMVP = new MAT_4_UNIFORM(controlHullProgram, "MVP");

    programUniforms.selectedVertex = new I_UNIFORM(controlHullProgram, "selectedVertex", -1);
    programUniforms.line = new I_UNIFORM(controlHullProgram, "line");
    glPointSize(5.0);

    programs.font = new PROGRAM(fontProgram);
    gui.font = new Font("res/textures/font.fnt", programs.font);
}

void generateTessTexture()
{
    float clearColor[4];

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBindFramebuffer(GL_FRAMEBUFFER, laplaceFBO);

    glGetFloatv(GL_COLOR_CLEAR_VALUE, clearColor);
    glClearColor(0.5, 0.5, 1.0, 1.0);
    //printf("aaa %d %d VBO: %d EBO: %d\n", FBO_WIDTH, FBO_WIDTH, laplaceVBO, laplaceEBO);

    glViewport(0, 0, FBO_WIDTH, FBO_WIDTH);
    programs.laplace->use();

    glDisable(GL_DEPTH_TEST);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,         laplaceVBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (GLvoid*) 0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (GLvoid*) (3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, laplaceEBO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);

    glEnable(GL_DEPTH_TEST);
    programs.bspline->use();

    glViewport(0, 0, gui.w, gui.h);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);
}

void drawTessTexture()
{
    programs.texView->use();

    glDisable(GL_DEPTH_TEST);
    if (gui.wireframe == true)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBindBuffer(GL_ARRAY_BUFFER,         laplaceVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (GLvoid*) 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (GLvoid*) (3*sizeof(float)));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, laplaceEBO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);

    if (gui.wireframe == true)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glEnable(GL_DEPTH_TEST);
    programs.bspline->use();
}

#ifdef _WIN32
#include <windows.h>

double getDeltaT()
{
	static uint64_t frekvencia = 0, cas1 = 0;
	unsigned __int64 cas2;
	double ret;
	if (frekvencia == 0)
		QueryPerformanceFrequency((LARGE_INTEGER*)&frekvencia);
	if (cas1 == 0)
	{
		QueryPerformanceCounter((LARGE_INTEGER *)&cas1);
		return 0.0;
	}
	QueryPerformanceCounter((LARGE_INTEGER *)&cas2);
	ret = (double)(cas2 - cas1)/frekvencia;
	cas1 = cas2;

	return ret;
}

#endif

#ifdef __gnu_linux__
#include <time.h>
#include <unistd.h>
double getDeltaT()
{
	static int64_t t1, t2, t;
	static time_t ts1, ts2, ts3;
	struct timespec ts;
	static int init = 1;
	if (init)
	{
		clock_gettime(CLOCK_REALTIME, &ts);
		ts1 = ts.tv_sec;
		t1 = (uint64_t)ts.tv_nsec;
		init = 0;
		return 0;
	}
	clock_gettime(CLOCK_REALTIME, &ts);
	t2 = (uint64_t) ts.tv_nsec;
	ts2 = ts.tv_sec;

	t = t2 - t1;
	ts3 = ts2 - ts1;

	t1 = t2;
	ts1 = ts2;

	return (double) t/1000000000 + ts3;
}
#endif

#define TRANS_MAX_SIZE 8000000
void drawOSD()
{
    GLuint primitives;
    static GLuint queries[2];
    static bool firstTime = true;
    static int c = 0;
    static int c2 = 0;
    static double delta;
    static double d = 0;

#ifdef GL_DEBUG
    static GLuint renderTime, transfBuffer, transfObject, renderTime2;


    if (firstTime)
    {
        programs.bspline->use();

        glGenQueries(2, queries);
        glGenBuffers(1, &transfBuffer);
        glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, transfBuffer);
        glBufferData(GL_TRANSFORM_FEEDBACK_BUFFER, TRANS_MAX_SIZE, NULL, GL_DYNAMIC_COPY);
        glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, transfBuffer, 0, TRANS_MAX_SIZE);

        glGenTransformFeedbacks(1, &transfObject);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transfObject);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, transfBuffer);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
        firstTime = false;
    }

#endif

    if   (gui.wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else                 glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


#ifdef GL_DEBUG

    glBeginQuery(GL_TIME_ELAPSED, queries[0]);

#else
    if (firstTime)
    {
        glGenQueries(2, queries);
        firstTime = false;
    }
    glBeginQuery(GL_PRIMITIVES_GENERATED, queries[1]);
#endif

    gui.obj->render(false);

#ifdef GL_DEBUG
    glEndQuery(GL_TIME_ELAPSED);
    glGetQueryObjectuiv(queries[0], GL_QUERY_RESULT, &renderTime);

    //glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, transfBuffer);

    //programs.bspline->use();
    glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transfObject);

    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, queries[1]);
    glBeginTransformFeedback(GL_TRIANGLES);
    gui.obj->render(false);

    glEndTransformFeedback();
    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
    //glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);


    glGetQueryObjectuiv(queries[1], GL_QUERY_RESULT, &primitives);


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    programs.simple->use();

    glBindBuffer(GL_ARRAY_BUFFER, transfBuffer);
//    float *data = (float*) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
//    for (int i = 0; i < 5; i++)
//        printf("%.3f %.3f %.3f %.3f\n", data[4*i], data[4*i + 1], data[4*i + 2], data[4*i + 3]);
//    glUnmapBuffer(GL_ARRAY_BUFFER);


    glBeginQuery(GL_TIME_ELAPSED, queries[0]);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (GLvoid*) (3*sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (GLvoid*) (6*sizeof(float)));
    glEnableVertexAttribArray(2);
    glDrawTransformFeedback(GL_TRIANGLES, transfObject);
    glEndQuery(GL_TIME_ELAPSED);
    glGetQueryObjectuiv(queries[0], GL_QUERY_RESULT, &renderTime2);

    float FPS = 1/(1e-09 * renderTime);

    printf("Distance: %.3f Primitives: %d\n renderTime: %d (%d) FPS: %.3f disp: %d\n", gui.distance, primitives, renderTime, renderTime2, FPS, gui.textI);
    printf("Trans Buff: %d\n", transfBuffer);


#else
    glEndQuery(GL_PRIMITIVES_GENERATED);
    glGetQueryObjectuiv(queries[1], GL_QUERY_RESULT, &primitives);
    char buff[256];

    sprintf(buff, "#Triangles: %d\n", primitives);
    gui.font->renderText((unsigned char*) buff, 0.1, 0.8, 0.1, (float) gui.w/gui.h);

    d += getDeltaT();
    c++;
    if (d > 0.1)
    {
        delta = d;
        c2 = c;
        d = 0;
        c = 0;
    }

    sprintf(buff, "FPS: %.3f\n", c2/delta);
    gui.font->renderText((unsigned char*) buff, -0.9, 0.8, 0.1, (float) gui.w/gui.h);
#endif
    if (gui.editing)
        gui.obj->renderControlHull(programUniforms.line);


}

void generateNormalMap()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBindFramebuffer(GL_FRAMEBUFFER, normalFBO);
    glViewport(0, 0, NORMAL_MAP_WIDTH, NORMAL_MAP_WIDTH);

    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);

    gui.obj->render(true);

    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, gui.w, gui.h);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glActiveTexture(GL_TEXTURE4);
    glGenerateMipmap(GL_TEXTURE_2D);
}

void updateMatrices()
{
    programUniforms.modelView->update( getModelViewMatrix());
    //programUniforms.projection->update(getProjectionMatrix());
    programUniforms.modelViewProjection->update(getModelViewProjectionMatrix());
#ifdef GL_DEBUG
    programUniforms.testMVP->update(getModelViewProjectionMatrix());
#endif

    programUniforms.controlMVP->update(programUniforms.modelViewProjection->value);
}

void reshape(GLFWwindow * window, int w, int h)
{
    glViewport(0, 0, w, h);
    perspective(60, (float) w/h, 0.1, 10.0);
    gui.w = w;
    gui.h = h;
}

int main()
{
    if (!glfwInit())
        return EXIT_FAILURE;

    glfwSetErrorCallback(error_callback);
    OpenSubdiv::Far::SetErrorCallback(callbackError);

    int w = 1280;
    int h = 720;

    GLFWwindow* window = glfwCreateWindow(w, h, "My Title", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return EXIT_FAILURE;

    }

    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    glfwSetWindowSizeCallback 	(window, reshape);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, mouse_motion_callback);
    glfwSetScrollCallback(window, scroll_callback);

    initGL();
    //initOSD();

    gui.obj = new Object("res/obj/sphere2.obj", programs.bspline, programs.normalMapGen, programs.control, 3, false);

    int i = 0;
    int err;
    reshape(window, w, h);
    while (!glfwWindowShouldClose(window))
    {

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        loadIdentity();

        translate(0.0, 0.0, -gui.distance);
        rotateY(gui.rY);
        rotateX(gui.rX);

        updateMatrices();
        generateNormalMap();
        generateTessTexture();

        switch (gui.textureView)
        {
        case 0:
            drawOSD();
            break;
        case 1:
            programUniforms.textureView->update(3);
            drawTessTexture();
            break;
        case 2:
            programUniforms.textureView->update(1);
            drawTessTexture();
            break;
        case 3:
            programUniforms.textureView->update(4);
            drawTessTexture();
            break;
        }

        glfwSwapBuffers(window);
        glfwPollEvents();

        err = glGetError();
        if (err != GL_NO_ERROR)
            fprintf(stderr, "%d: GL error: %d (%s)\n", i, err, gluErrorString(err));

        i++ ;
    }


    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
