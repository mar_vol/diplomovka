#ifndef _SHADER_H_
#define _SHADER_H_

#define GLEW_STATIC
#include <GL/glew.h>

//#define GL_DEBUG 1

GLuint loadShaders(const char* filePath, bool tesselation = false, bool geometry = false, bool feedback = false);
GLuint attachShader(const GLuint programID, const GLuint type, const char* filePath, const char* tessellationShaderCommonPath = NULL);

#endif // _SHADER_H_
