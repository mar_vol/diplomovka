#ifdef VERTEX_SHADER

in layout(location = 0) vec3 pos;
in layout(location = 1) vec2 uv;

out vec2 Uv;

void main(void)
{
    gl_Position = vec4(pos, 1.0);
    Uv = uv;
}
#endif

#ifdef FRAGMENT_SHADER
uniform sampler2D inputTexture;
in vec2 Uv;
out vec4 color;

void main()
{
    float c = texture(inputTexture, Uv).r;
    color = vec4(c, c, c, 1.0);

    color = texture(inputTexture, Uv);
}

#endif
