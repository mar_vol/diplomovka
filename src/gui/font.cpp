#include "font.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

Font::Font(const char* fontFile, PROGRAM* fontProgram):fStart(fontProgram->programID, "fStart"), fBase(fontProgram->programID, "fBase"), fHeight(fontProgram->programID, "fHeight"), fAspect(fontProgram->programID, "fAspect"), posT(fontProgram->programID, "pos")
{
    FILE* fFile = fopen(fontFile, "rb");
    char name[256];
    char value[30];
    int c;
    int s = 0;
    int i = 0, j = 0, w, h, id, x, y, xo, yo, w2, h2, xa, lHeight, b;

    fontText = NULL;

    if (fFile == NULL)
    {
        fprintf(stderr, "Nemozem otvorit: %s\n", fontFile);
        getchar();
    }

    memset(charBuffer, 0, sizeof(charBuffer));

    do
    {
        c = fgetc(fFile);
        switch (c)
        {
            case ' ':
            case '\r':
            case '\t':
            case '\n':
            case EOF:
                if (s == 1)
                {
                    s = 0;
                    value[j] = '\0';
                    i = 0;

                    if (!strcmp(name, "file") && j > 0)
                    {
                        glActiveTexture(GL_TEXTURE5);
                        if (value[0] == '\"')
                        {

                            for (i = 1; value[i] != '\"' && value[i] != EOF && value[i] != 0; i++);

                            if (value[i] == '\"')
                                value[i] = '\0';

                            fontTexture = generateTexture(value + 1);
                            i = 0;
                        }
                        else
                            fontTexture = generateTexture(value);
                    }
                    else if (!strcmp(name, "scaleW"))
                    {
                        sscanf(value, "%d", &w);
                    }
                    else if (!strcmp(name, "scaleH"))
                    {
                        sscanf(value, "%d", &h);
                    }
                    else if (!strcmp(name, "id"))
                    {
                        sscanf(value, "%d", &id);
                    }
                    else if (!strcmp(name, "x"))
                    {
                        sscanf(value, "%d", &x);
                    }
                    else if (!strcmp(name, "y"))
                    {
                        sscanf(value, "%d", &y);
                    }
                    else if (!strcmp(name, "base"))
                    {
                        sscanf(value, "%d", &b);
                    }
                    else if (!strcmp(name, "lineHeight"))
                    {
                        sscanf(value, "%d", &lHeight);
                    }
                    else if (!strcmp(name, "width"))
                    {
                        sscanf(value, "%d", &w2);
                    }
                    else if (!strcmp(name, "height"))
                    {
                        sscanf(value, "%d", &h2);
                    }
                    else if (!strcmp(name, "xoffset"))
                    {
                        sscanf(value, "%d", &xo);
                    }
                    else if (!strcmp(name, "yoffset"))
                    {
                        sscanf(value, "%d", &yo);
                    }
                    else if (!strcmp(name, "xadvance"))
                    {
                        sscanf(value, "%d", &xa);
                    }
                    else if (!strcmp(name, "chnl"))
                    {
                        //printf("w: %d h: %d id: %d x: %d y: %d w2: %d h2: %d xo: %d yo: %d\n", w, h, id, x, y, w2, h2, xo, yo);
                        if (id >= 0 && id < 256)
                        {
                            charBuffer[id].u = (float) x/w;
                            charBuffer[id].v = (float) (h - h2 - y)/h;

                            charBuffer[id].u2 = (float) (x + w2)/w;
                            charBuffer[id].v2 = (float) (h - y)/h;

                            charBuffer[id].x = (float) xo/w;
                            charBuffer[id].y = (float) (b - yo - h2)/h;

                            xadvanceBuffer[id] = (float) xa/w;
                        }
                        //printf("u: %.3f v: %.3f u2: %.3f v2: %.3f x: %.3f y: %.3f\n", charBuffer[id].u, charBuffer[id].v, charBuffer[id].u2, charBuffer[id].v2, charBuffer[id].x, charBuffer[id].y);

                    }

                }
                else
                {
                    i = 0;
                }
                break;
            case '=':
                name[i] = '\0';
                s = 1;
                j = 0;
                break;
            default:
                if (s == 0)
                    name[i++] = c;
                else
                    value[j++] = c;
        }
    }while (c != EOF);

    fclose(fFile);

    lastSize = 0;
    lineHeight = (float) lHeight/h;
    base = (float) b/h;

    fBase.update(base);
    posT.update(6);
    printf("lHeight: %.3f base: %.3f\n", lineHeight, base);

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
        glGenBuffers(1, &VBO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(charBuffer), charBuffer, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(charBuffer[0]), (GLvoid*) 0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(charBuffer[0]), (GLvoid*) (2*sizeof(float)));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(charBuffer[0]), (GLvoid*) (4*sizeof(float)));
        glEnableVertexAttribArray(2);

        glActiveTexture(GL_TEXTURE6);
        glGenTextures(1, &posTexture);
        glBindTexture(GL_TEXTURE_1D, posTexture);
        //glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        //glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glTexImage1D(GL_TEXTURE_1D, 0, GL_R16F, 256, 0, GL_RED, GL_FLOAT, NULL);


        glGenBuffers(1, &EBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 256, NULL, GL_DYNAMIC_DRAW);

        glActiveTexture(GL_TEXTURE0);

    glBindVertexArray(0);

    this->fontProgram = fontProgram;
    fontText = new I_UNIFORM(fontProgram->programID, "fontText", 5);
}
Font::~Font()
{
    glDeleteTextures(1, &posTexture);
    glDeleteTextures(1, &fontTexture);
    delete fontText;
}

void Font::renderText(const unsigned char* text, float x, float y, float height, float aspect)
{

    int len = strlen((const char*) text);
    int i;
    unsigned char* text2;
    static float posBuffer[256];
    float pos = 0;

    for (i = 0; i < len; i++)
    {
        posBuffer[i] = pos;
        pos += xadvanceBuffer[text[i]];
    }
    glGetError();
    glDisable(GL_DEPTH_TEST);
    fontProgram->use();

    float temp[] = {x, y};
    fStart.update(temp);
    fHeight.update(height);
    fAspect.update(aspect);


    glBindVertexArray(VAO);
    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    text2 = (unsigned char*) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
    strcpy((char*)text2, (const char*)text);
    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

    glActiveTexture(GL_TEXTURE6);
    glTexSubImage1D(GL_TEXTURE_1D, 0, 0, len, GL_RED, GL_FLOAT, posBuffer);

    glDrawElements(GL_POINTS, len, GL_UNSIGNED_BYTE, NULL);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(0);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
}


int generateTexture(const char* const file, bool mipmap, int nullW, int nullH)
{
    /*vygeneruje texturu zo suboru bmp, nastavi filtrovanie na mipMapy a vrati jej identifikator*/
    unsigned char *data = NULL;
    int w, h, mode;
    unsigned int  tex;
    bool null = (file == NULL);

    if (!null && ((data = loadBMP(file, &w, &h, &mode)) == NULL))
    {
        fprintf(stderr, "Nemozem nacitat: %s", file);
        getchar();
        return -1;
    }

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    if (mipmap)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    else
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    if (null)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, nullW, nullH, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    else
    {
        if (mode != GL_LUMINANCE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,     w,     h, 0,    mode, GL_UNSIGNED_BYTE, data);
        else
            glTexImage2D(GL_TEXTURE_2D, 0, mode,     w,     h, 0,    mode, GL_UNSIGNED_BYTE, data);
        free(data);
    }


    fprintf(stderr, "Error: %d\n", glGetError());

    if (mipmap)
        glGenerateMipmap(GL_TEXTURE_2D);

    return tex;

}

unsigned char* loadBMP(const char* const f, int* w, int* h, int* mode)
{
    //nacita obrazok bmp do textury
    FILE *file = fopen(f, "rb");
    BMP bmp;
    int pad, i;
    unsigned char *data;
    if (file == NULL)
    {
        printf("Neda sa otvorit: %s\n", f);
        return NULL;
    }
    fread(&bmp, 1, sizeof(BMP), file);
    if (bmp.Mn != 19778)
    {
        printf("%s nie je platny .bmp subor\n", f);
        return NULL;
    }
    fseek (file, bmp.Offset , SEEK_SET );
    switch(bmp.BitsPerPixels)
    {
    case 32:
        *w = bmp.Width;
        *h = bmp.Height;
        *mode = GL_BGRA;
        data = (unsigned char*)malloc(sizeof(unsigned char) * (*w) * (*h) * 4);
        fread(data, 1, (*w)*(*h)*4, file);
        break;
    case 24:
        *w = bmp.Width;
        *h = bmp.Height;
        *mode = GL_BGR;

        data = (unsigned char*)malloc(sizeof(unsigned char) * (*w) * (*h) * 3);
        if (((*w)*3) & 3)
            pad = 4 - (((*w)*3) & 3);
        else
            pad = 0;
        for (i = 0; i < (*h); i++)
        {
            fread(data + i*(*w)*3, 1, (*w)*3, file);
            fseek(file, pad, SEEK_CUR);
        }
        break;
    case 8:
        *w = bmp.Width;
        *h = bmp.Height;
        *mode = GL_LUMINANCE;

        data = (unsigned char*)malloc(sizeof(unsigned char) * (*w) * (*h));
        fread(data, 1, (*w)*(*h), file);
        break;
    default:
        printf("Nepodporovany format bmp: %s\n", f);
        getchar();
        fclose(file);
        return NULL;
    }
    fclose(file);
    return data;
}


