#ifndef _GUI_H_
#define _GUI_H_

struct GUI
{
    int   _x, _y;
    float rX, rY;
    float distance;
    int rotButtonPress;
    int rotButtonPressInit;
    int w, h;
    int textureView;
    int wireframe;

    int currentDisplaceTexture;
    int displaceTextureCount;
    int displaceTextures[8];
    Object* obj;

    int text[3];
    int textI;

    bool moving;
    bool editing;

    Font* font;

    GUI()
    {
        rX = 0.0;
        rY = 0.0;
        rotButtonPress = 0;
        rotButtonPressInit = 0;
        distance = 1;
        textureView = 0;
        wireframe = 0;

        currentDisplaceTexture = 0;
        displaceTextureCount = 0;
        memset(displaceTextures, 0, sizeof(displaceTextures));
        obj = NULL;

        textI = 3;
        moving = false;
        editing = false;
        font = NULL;
    }

    ~GUI()
    {
        delete obj;
        delete font;
    }

} gui;

struct PROGRAM_UNIFORMS
{
    MAT_4_UNIFORM* modelView;
    MAT_4_UNIFORM* projection;
    MAT_4_UNIFORM* modelViewProjection;
    MAT_4_UNIFORM* testMVP;
    MAT_4_UNIFORM* controlMVP;

    F_UNIFORM* dispStrength;
    F_UNIFORM* dispMidStrength;
    F_UNIFORM* tessFactor;
    I_UNIFORM* textureView;
    I_UNIFORM* tessType;
    F_UNIFORM* tessOuter;
    I_UNIFORM* simpleText;
    F_UNIFORM* t;

    F_UNIFORM* dispStrengthNM;
    F_UNIFORM* dispMidStrengthNM;
    F_UNIFORM* tessFactorNM;
    F_UNIFORM* tNM;

    I_UNIFORM* selectedVertex;
    I_UNIFORM* line;

    VEC3_UNIFORM* materialAmbient;
    VEC3_UNIFORM* materialDiffuse;
    VEC3_UNIFORM* materialSpecular;
    F_UNIFORM*    materialShininess;
    F_UNIFORM*    materialReflection;

    VEC3_UNIFORM* sunAmbient;
	VEC3_UNIFORM* sunDiffuse;
	VEC3_UNIFORM* sunSpecular;
    VEC3_UNIFORM* sunDirection;

    PROGRAM_UNIFORMS()
    {
        modelView = NULL;
        projection = NULL;
        dispStrength = NULL;
        dispMidStrength = NULL;
        textureView = NULL;
        tessType = NULL;
        tessFactor = NULL;
        tessOuter = NULL;
        dispStrengthNM = NULL;
        dispMidStrengthNM = NULL;
        tessFactorNM = NULL;
        modelViewProjection = NULL;
        testMVP = NULL;
        simpleText = NULL;
        controlMVP = NULL;
        selectedVertex = NULL;
        line = NULL;

        materialAmbient = NULL;
        materialDiffuse = NULL;
        materialSpecular = NULL;
        materialShininess = NULL;
        materialReflection = NULL;

        sunAmbient   = NULL;
        sunDiffuse   = NULL;
        sunSpecular  = NULL;
        sunDirection = NULL;

        t = NULL;
        tNM = NULL;
    }
    ~PROGRAM_UNIFORMS()
    {
        delete modelView;
        delete projection;
        delete modelViewProjection;
        delete dispStrength;
        delete dispMidStrength;
        delete textureView;
        delete tessType;
        delete tessFactor;
        delete tessOuter;
        delete dispStrengthNM;
        delete dispMidStrengthNM;
        delete tessFactorNM;
        delete testMVP;
        delete simpleText;
        delete controlMVP;
        delete selectedVertex;
        delete line;

        delete materialAmbient;
        delete materialDiffuse;
        delete materialSpecular;
        delete materialShininess;
        delete materialReflection;

        delete sunAmbient;
        delete sunDiffuse;
        delete sunSpecular;
        delete sunDirection;

        delete t;
        delete tNM;
    }
} programUniforms;

struct PROGRAMS
{
    PROGRAM* def;
    PROGRAM* bspline;
    PROGRAM* laplace;
    PROGRAM* texView;
    PROGRAM* normalMapGen;
    PROGRAM* simple;
    PROGRAM* control;
    PROGRAM* font;

    PROGRAMS()
    {
        def = new PROGRAM(0);
        bspline = NULL;
        laplace = NULL;
        texView = NULL;
        normalMapGen = NULL;
        simple = NULL;
        control = NULL;
        font = NULL;
    }

    ~PROGRAMS()
    {
        delete def;
        delete bspline;
        delete laplace;
        delete texView;
        delete normalMapGen;
        delete simple;
        delete control;
        delete font;
    }


} programs;

#endif // _GUI_H_
